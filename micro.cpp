#include "micro.hpp"
#include "micro_tools.hpp"


int main(int argc, char** argv)
{
  // process and store command line parameters, init global variables 
  Args args(argc, argv);
  
  // load miRNA database
  if (!args.referenceFileName.empty () && !args.gffFileName.empty ()) { // fastq + gff
    cerr << "Loading reference and miRNAs..." << flush ;
    if (!args.db.read_gff (args.gffFileName)) {
      cerr << "failed: gff file cannot be opened" << endl;
      exit (EXIT_FAIL);
    }
    int retval = args.db.read_sequences (args.referenceFileName);
    if (retval == -2) {
      cerr << "failed: reference miRNA definition out of range" << endl;      
      exit (EXIT_FAIL);
    }
    if (retval == -1) {
      cerr << "failed: reference file cannot be opened" << endl;      
      exit (EXIT_FAIL);
    }
    if (retval > 0) {
      cerr << "failed: " << retval << " miRNA definition did not match reference chromosomes" << endl;
      exit (EXIT_FAIL);
    }
    args.db.a2adist (); // build internal data structure - load () does not need calling this
    cerr << "done" << endl;
    if (!args.mirnaFileName.empty ()) {  //  saving the db of mirna was required
      if (!args.db.store (args.mirnaFileName)) {
        cerr << "Error writing miRNA db file" << endl;      
        exit (EXIT_FAIL);
      }
    }
    else { //  raise a suggestion to the standard error
      cerr << "[INFO] - You can save processing time specifying the --mir-db option (see manual)" << endl;
    }
    if (args.readsFileName.empty ()) exit (EXIT_OK);  //  Only db creation was required
  }
  else {   //  load sequences and internal data structure from db
    if (!args.db.load (args.mirnaFileName)) {
      cerr << "Error reading miRNA db file" << endl;      
      exit (EXIT_FAIL);
    }
  }
  if(args.verbose>0) 
    cerr << args.db.keys().size() << " miRNA sequences\n";
  // extract the ids of sequences which are minimal (not containing any other sequence)
  args.minimal_ids = args.db.ids (true);   // list of minimal miRNA ids
  //  set the minimum length of (trimmed) reads to match a miRNA
  args.min_read_length = args.db.minseq_len () - (MAX_SKIP + Max_agrep_errors);

  if(args.verbose>0) {
    cerr << args.minimal_ids.size () << " miRNA sequences are unique\n";
    cerr << "Reads less than " << args.min_read_length << " bps are marked as too short\n";
  }
  // build the map<string, int> sequence, id for minimal sequences
  // XXX - It seems to be used only by trie check ()
  for (int i = 0; i < (int) args.minimal_ids.size (); i ++) {
    mirna_t *mirna = args.db[args.minimal_ids[i]];
    args.seq_id[mirna->sequence] = mirna->id;
  }
  
  // init trie data structure
  TrieNode::set_alpha(args.alphabet);
  // build trie for mirna sequences  
  TrieNode *root = build_trie(args,false);
  int links = root->compute_suffix_links();
  args.trie_root = root;  
  if(args.verbose>0)
    cerr << "inserted " << links << " suffix links\n--\n";

  // build trie for reverse complement sequences if required
  if(args.revcompl) {
    TrieNode *root = build_trie(args,true);
    int links = root->compute_suffix_links();
    args.trie_rc_root = root;
    if(args.verbose>0)
      cerr << "inserted " << links << " suffix links (rev compl seqs)\n--\n";
  }
   
  // visit the trie and check that sequences and ids match with those in the map (DEBUG)
  assert(root->check(args.seq_id));
  if(args.verbose>1) {
    cerr << "Strings retrieved from the trie in lex order:\n";
    int tot =  root->dump();
    cerr << "Total: " << tot << endl;
  }
  cerr << "==== Trie construction completed\n";
  cerr << "==== Elapsed time: " << difftime(time(NULL),args.start_time) << " wall clock seconds\n";        
  if (args.trim_adapter) {  // this is false if user specified --no-trim-adapter
    //  Regardless the adapter is provided or guessed we need the adapter object
    assert(args.myadapter!=NULL); // was created in args constructor
    if (args.adapter_sequence.empty ()) {  // Adapter not provided, guess it
      // first pass with exact search only to determine the adapter   
      bool more_reads = get_adapter_stats(args);
      cerr << "==== Adapter data collected\n";
      if(more_reads) {
        cerr << " Cannot proceed: Not enought exact matches for determining the adapter\n";
        cerr << " Exiting...\n";
        exit(EXIT_FAIL);
      }
      //  Set the adapter in args 
      args.adapter_sequence = args.myadapter->guess_adapter ();
      // report on the search of the adapter
      if (args.verbose > 0) {
        cerr << "==== Adapter dump:\n";
        args.myadapter->dump (args.verbose);
      }
      cerr << "==== Elapsed time: " << fixed << setprecision(2) << difftime(time(NULL),args.start_time) << " wall clock seconds\n"; 
      cerr << "==== Full adapter guess: " << args.adapter_sequence << endl;
      //  Convert adapter in uppercase to search it in the reads
      for_each (args.adapter_sequence.begin (), args.adapter_sequence.end (), [](char &c) {c = toupper (c); } );
    }
    else {  // Adapter provided
      cerr << "==== Full user-provided adapter: " << args.adapter_sequence << endl;  
    }
  
    // compute the shortest prefix of the full adapter which is at edit-distance  
    // *greater* than adapter_inread_distance 
    // ie the adapter is guaranteed to be at distance greater than this to every micro (alse reverse complement?) 
    tuple<int, int, string> aml = args.myadapter->_adapter_min_length (args.db, args.adapter_sequence, args.adapter_inread_distance);
    if (get<1>(aml) <=  args.adapter_inread_distance) {
      cerr << " Cannot select an adapter at distance > " << args.adapter_inread_distance;
      cerr << " from the set of micros" << endl;
      cerr << " Full adapter and micro " << get<2>(aml) << " are at distance " << get<1>(aml) << endl;
      cerr << " Exiting...\n";
      exit(EXIT_FAIL);
    }
    int safe_adapter_length =get<0>(aml);
    cerr << "==== Adapter length to ensure edit-distance from micros > " << args.adapter_inread_distance;
    cerr << " is " << safe_adapter_length << endl;
    // save the safe adapter data
    args.safe_adapter = args.adapter_sequence.substr(0,safe_adapter_length);
    args.safe_adapter_length = safe_adapter_length;
  }
  else {  //  this is the case where --no-trim-adapter was set.
    assert(args.myadapter==NULL);  
    args.safe_adapter = "";        //  dummy value for process_reads ()
    args.safe_adapter_length = 0;  //  dummy value for process_reads ()
  }
  // end of adapter guess or user setting
  
  // args.max_adapter_errors = adapter_micro_distance;
  
  // XXX  unless adapter searching was disabled by user  XXX
  // search the adapter in the reads with at most adapter_micro_distance errors 
  // the above procedure guarantees that the adapter will not match with a micro
  // once we have found the adapter, the micros are searched within the trimmed reads
  process_reads(args); 
  // save/dump counts 
  if (args.mirna_counting) {  //  mirna_counting is false if counting was disabled
    if (args.countsFileName.empty ())  //  dump
      args.db.save_counts (std::string (), '\t', args.counts_header, args.ambiguous_column, args.correct_with_ambiguous);
    else  //  save
      args.db.save_counts (args.countsFileName, '\t', args.counts_header, args.ambiguous_column, args.correct_with_ambiguous);
  }

  //  Display summary
  args.display_statistics ();
  return EXIT_OK;
}

//   Add non-minimal matching miRNAs
void find_additional_matches (vector<HDmatch> &found, singleRead *r, Args *args) {
  set<int> proc_ids; //  Used to avoid to put duplicates in HDmatch vector
  vector<HDmatch>::size_type tot_elems = found.size ();
  for (vector<HDmatch>::size_type i = 0; i < tot_elems; i ++) { //  Check all results
    bool revcompl = found[i].revcompl;
    int start = found[i].start;
    mirna_t *mir_record = args->db[found[i].id];
    if (proc_ids.count (mir_record->id) != 0) continue;  //  It is a duplicate
    else proc_ids.insert (mir_record->id);
    for (auto subs = mir_record->is_subsequence_of.begin (); subs != mir_record->is_subsequence_of.end (); subs ++) {
      mirna_t *alt_mir = args->db[subs->first]; // subs iterate map<string, size_t>
      if (proc_ids.count (alt_mir->id) != 0) continue;  //  It is a duplicate
      else proc_ids.insert (alt_mir->id);
      string alt_seq = alt_mir->sequence;
      int offset = start - subs->second;   // subs iterate map<string, size_t>
      if (revcompl) alt_seq = StringTools::revcompl (alt_seq, true);
      if (offset < 0) {
  alt_seq = alt_seq.substr (-offset);
  offset = 0;
      }
      pair<u_int16_t, u_int16_t> h = StringTools::mismatches_rm (r->dna, r->quality, offset, alt_seq);
      HDmatch new_match(offset, alt_mir->sequence.size (), h.first, alt_mir->id, revcompl, h.second);
      found.push_back (new_match);
    }
  }
}

unsigned int mark_best_match (vector<HDmatch> &found, singleRead *r, Args *args) {
  unsigned int tot_matches;
  if (found.size () == 1) { //  Only one result
    found[0].best_match = true;
    return 1;  
  }
  sort (found.begin(), found.end());
  // Check if there is only one possible best match - done just for efficiency
  if ((found[0] == found[1]) == false) {
    found[0].best_match = true;
    return 1;  
  }
  //  This point is reached only when at least two results are equally good and
  //  we need to use quality as a discriminant
  int min_gap_score = -1;
  for (unsigned int i = 0; i < found.size () && found[i] == found[0]; i ++) {
    if (found[i].gap_score == -1) {  //  Compute the gap quality
      string mir_seq = args->db[found[i].id]->sequence;
      if (found[i].revcompl) mir_seq = StringTools::revcompl (mir_seq, true);
      // h = hammingD, gap_score
      pair<u_int16_t, u_int16_t> h = StringTools::mismatches_rm (r->dna, r->quality, found[i].start, mir_seq);
      assert (found[i].hammingD == h.first);
      found[i].gap_score = h.second;
    }
    //  Compute the min score   XXX - min_gap_score = -1 in the initialization
    if (min_gap_score == -1 || found[i].gap_score < min_gap_score) min_gap_score = found[i].gap_score;
  }
  //  Set best matches
  tot_matches = 0;
  for (unsigned int i = 0; i < found.size () && found[i] == found[0]; i ++) {
    if (found[i].gap_score == min_gap_score) { 
      found[i].best_match = true;  
      tot_matches ++;
    }
  }
  return tot_matches;
}

// report to the proper output file the matches in found.
// For each match dna and q are the original read and its quality score (even if the match is with the revcompl)
// revcompl == true means that the match was with dna reverse-complemented
void cout_match_report (vector<HDmatch> &found, singleRead *r, Args *args) {
  // if many threads lock cout when reporting debug information
  if(args->helper_threads>1)
    xpthread_mutex_lock(&args->thread_info->mutex_cout,__LINE__,__FILE__);

  cout << r->header << endl;
  cout << r->dna << endl;
  cout << r->quality << endl;
  unsigned int i;
  for (i = 0; i < found.size () && found[i].best_match == false; i ++);
  assert (found[i].best_match);
  string s = args->db[found[i].id]->sequence;
  if(found[i].revcompl) s = StringTools::revcompl (s, true);
  if(found[i].start<0)
    cout << setw(found[i].start + found[i].len) << s.substr(-found[i].start, found[i].len + found[i].start) << endl;
  else 
    cout << setw(found[i].start + found[i].len) << s << endl;
  for (auto it = found.begin (); it != found.end (); it ++) {
    cout << (it->revcompl ? " - " : " + ");  // - if reverse complement, + otherwise
    cout << setw(2) << it->hammingD;
    cout << setw(4) << it->start;
    cout << setw(4) << it->len;
    cout << " " << args->db[it->id]->name;
    if (it->best_match == true) cout << " *";
    if (it->gap_score != -1)  cout << " " << it->gap_score;
    cout << endl;
  }
  cout << "-----\n";    
  if(args->helper_threads>1)
    xpthread_mutex_unlock(&args->thread_info->mutex_cout,__LINE__,__FILE__);
  
}

void update_counts (vector<HDmatch> &found, unsigned int tot_best_matches, Args *args) {
  unsigned int reported = 0;
  for (unsigned int i = 0; i < found.size () && reported < tot_best_matches; i ++) {
    if (!found[i].best_match) continue;
    mirna_t *mirna = args->db[found[i].id];
    if (tot_best_matches == 1) mirna->count ++;  //  This is atomic thus thread safe
    else mirna->ambiguous_count ++;  //  This is atomic thus thread safe
    reported ++;
  }
}


// save to the proper file (indicated in args) the data of a matching read
void save_matched_read (vector<HDmatch> &found, unsigned int tot_best_matches, singleRead *r, Args *args)
{
  bool dump_read = true;
  if (tot_best_matches == 1 && args->matching_report == false) dump_read = false;
  if (tot_best_matches > 1 && args->ambiguous_report == false) dump_read = false;
  int min_hammingD = -1;  // Used for statistics
  //  Update processing statistics
  args->stats_reads ++;
  args->stats_match ++;
  if (tot_best_matches != 1) args->stats_amb ++;
  if (found[0].revcompl) args->stats_rc ++;
  // lock the output file when reporting the matches in found 
  if(args->helper_threads>1)
    xpthread_mutex_lock(&args->thread_info->mutex_fout,__LINE__,__FILE__);
  if (args->fout.is_open() && dump_read) {  //  User required output fastq
    if (tot_best_matches == 1) args->fout << "@" << r->header << Matching_found_message;
    else args->fout << "@" << r->header << Ambiguous_matching_message;
  }
  unsigned int reported = 0;
  for (unsigned int i = 0; i < found.size () && reported < tot_best_matches; i ++) {
    if (!found[i].best_match) continue;
    if (min_hammingD == -1) min_hammingD = found[i].hammingD; //  Initialization
    if (found[i].hammingD < min_hammingD) min_hammingD = found[i].hammingD;  //  Update
    if (args->fout.is_open() && dump_read) {  //  User required output fastq
      args->fout << " " << args->db[found[i].id]->name << ":" << found[i].start;
      args->fout << (found[i].revcompl ? ":-:" : ":+:");  // - if reverse complement, + otherwise
      args->fout << found[i].hammingD;
    }
    reported ++;
  }
  if (args->fout.is_open() && dump_read) {  //  User required output fastq
    if (args->outRNA)
      args->fout << "\n" << StringTools::asRNA (r->dna) << "\n+\n" << r->quality << endl;
    else
      args->fout << "\n" << r->dna << "\n+\n" << r->quality << endl;
  }
  //  Unlock the output file when reporting the matches in found 
  if(args->helper_threads>1)
    xpthread_mutex_unlock(&args->thread_info->mutex_fout,__LINE__,__FILE__);

  //  Update processing statistics
  if  (min_hammingD == 0) args->stats_perfect ++;
}


// save to the proper file (indicated in args) the data of a read
// that did not match any miRNA, for possible additional processing  
void save_unmatched_read(singleRead *r, Args *args, char msg_code) 
{
  //  Update processing statistics
  args->stats_reads ++;
  switch (msg_code) {
  case Adapter_not_found:
    args->stats_no_adapt ++;
    break;
  case Micro_not_found:
    args->stats_unmatch ++;
    break;
  case Read_too_short:
    args->stats_short ++;
    break;    
  case Only_trimming:  //  This is the case where micro is not searched
    break;
  default:
    assert (0==1); //  I should trow an exception
    break;
  }
  //  After updating statistics, return if reporting is not required 
  if (!args->fout.is_open()) return;  //  User didn't require output fastq
  if (msg_code == Adapter_not_found && args->no_adapt_report == false) return;
  if (msg_code == Micro_not_found   && args->mismatch_report == false) return;
  if (msg_code == Read_too_short    && args->tooshort_report == false) return;
  //  write fastq entry
  if(args->helper_threads>1)
    xpthread_mutex_lock(&args->thread_info->mutex_fout,__LINE__,__FILE__);
  switch (msg_code) {
  case Adapter_not_found:
    args->fout << "@" << r->header << Adapter_not_found_message << "\n";
    break;
  case Micro_not_found:
    args->fout << "@" << r->header << Micro_not_found_message << "\n";
    break;
  case Read_too_short:
    args->fout << "@" << r->header << Read_too_short_message << "\n";
    break;
  case Only_trimming:  //  In this case print the header as is
    args->fout << "@" << r->header << "\n";
    break;
  default:
    assert (0==1); //  I should trow an exception
    break;
  }
  if (r->dna[0] != '\0') {
    if (args->outRNA)
      args->fout << StringTools::asRNA (r->dna) << "\n+\n" << r->quality << endl;
    else
      args->fout << r->dna << "\n+\n" << r->quality << endl;
  }
  else { //  the case of empty read because adapter was at the first position
    if (args->base33qual) 
      args->fout << "N\n+\n!" << endl;  //  common base33 quality
    else
      args->fout << "N\n+\n@" << endl;  //  old Illumina base64 quality
  }      
  if(args->helper_threads>1)
    xpthread_mutex_unlock(&args->thread_info->mutex_fout,__LINE__,__FILE__);
}



//process a single read; return true if at least one match is found 
void process_single_read(singleRead *r, Args *args)
{
  TrieNode *root = args->trie_root;
  TrieNode *root_rc = args->trie_rc_root;
  vector<HDmatch> found;
  size_t read_length = strlen (r->dna); // just for statistics
  size_t trimmed = 0;
  args->stats_bp += read_length;   // update stats on the total processed bps (variable os atomic)
  
  // search the adapter in the read if required 
  if(args->trim_adapter) {
    assert(args->safe_adapter_length>0);
    pair<int,int> res = Agrep::search_edit_distance(r->dna,args->adapter_mask,args->safe_adapter_length,args->adapter_inread_distance);
    // if the adapter was not found, report the read as umnatched with annotation Adapter not foound
    if(res.second==0) {
      save_unmatched_read(r,args,Adapter_not_found); // Update stats + save if required
      return;
    }
    assert(res.first <= args->adapter_inread_distance); // # of errors within the limit 
    assert(res.second + res.first >= args->safe_adapter_length); // match not shorter than adapter_len-errors 
    assert(res.second<= (int) strlen(r->dna)); // matched region shorter than read
    // cut read at the beginning of the adapter match 
    int pre_adapter_len = res.second - args->safe_adapter_length + res.first; // max lenght of unmatched region before the adapter
    r->dna[pre_adapter_len] = '\0';
    if(pre_adapter_len <= (int) strlen(r->quality))
      r->quality[pre_adapter_len] = '\0';
    trimmed = read_length - pre_adapter_len;
  }
  else assert(args->safe_adapter_length==0);

  args->stats_trimmed_bp += trimmed;   // update stats on the total trimmed bps (variable is atomic)
  //  Handle reads too short 
  if (read_length - trimmed < args->min_read_length) {
    save_unmatched_read(r,args, Read_too_short); // Update stats + save if required
    return;
  }
  if (!args->mirna_counting) {  //  Only adapter trimming was required
    save_unmatched_read(r,args,Only_trimming); // Update stats + save if required
    return;
  }

  // ---------------- exact search    
  pair<int,int> res = root->search(r->dna);
  if(res.first>=0) {                            // res.first is id of seq
    int len = args->db[res.first]->sequence.size(); // length of the match 
    int start = res.second - len +1;            // starting position of match 
    HDmatch match(start,len,0,res.first,false); // create 0-error match
    found.push_back(match);
  }
  // search in the reverse complement trie if requested 
  if(args->revcompl) {
    res = root_rc->search(r->dna);
    if(res.first>=0) {
      int len = args->db[res.first]->sequence.size(); // length of the match 
      int start = res.second - len +1;            // starting position of match 
      HDmatch match(start,len,0,res.first,true);  // create 0-error match, with reverse complement  
      found.push_back(match); 
    }
  }
  // if there are some exact matches, report them and exit
  if(found.size()>0) {
    find_additional_matches (found,r,args);
    unsigned int tot_best_matches = mark_best_match (found, r, args);
    //if (found.size () > 1) cout_match_report (found, r, args);
    update_counts (found, tot_best_matches, args);  //  Update counts in the db
    save_matched_read (found,tot_best_matches,r,args);
    return;
  }
  
  // check if the current trimmed read was already seen and discarded
  if(args->discarded_rejected_dup>=0) { // if <0 we do not use args->rejected
    if(args->helper_threads>1)     
      xpthread_mutex_lock(&args->thread_info->mutex_rejected,__LINE__,__FILE__);
    bool first_time = (args->rejected.find(r->dna)==args->rejected.end());
    if(args->helper_threads>1)     
      xpthread_mutex_unlock(&args->thread_info->mutex_rejected,__LINE__,__FILE__);
    if(!first_time) {
      args->discarded_rejected_dup++; // using the flag to count how many have been skipped
      // maybe change of remove this call 
      save_unmatched_read(r,args, Micro_not_found); // Update stats + save if required
      return;
    }
  }
  // ------ inexact search
  // search hamming distance > 0 if requested  
  if(args->maxmism>0) {
    vector<HDmatch> found;
    int tot_found = 0;
    for(unsigned i=0; i< args->minimal_ids.size (); i++) {
      int id_i = args->minimal_ids[i];
      int newly_found = Agrep::search(r->dna, args->db[id_i]->sequence.c_str(), args->maxmism, args->skip, found);
      for(int j=0;j<newly_found;j++)
        found[tot_found++].setIdRc(id_i,false); // set id of micro and that it is not a rev-complement
    }
    // search revcompl for hamming distance > 0 if requested  
    if(args->revcompl) {     
      for(unsigned i=0; i< args->minimal_ids.size (); i++) {
        int id_i = args->minimal_ids[i];
        string seq = StringTools::revcompl(args->db[id_i]->sequence,true);
        int newly_found = Agrep::search(r->dna, seq.c_str(), args->maxmism, args->skip, found);
        for(int j=0;j<newly_found;j++)
          found[tot_found++].setIdRc(id_i,true); // set id of micro and that it is a rev-complement
      }
    }

    // report matches with errors
    if(found.size()>0) {
      find_additional_matches (found,r,args);      
      //sort(found.begin(),found.end());  // Only if mark_best_match () is not called
      unsigned int tot_best_matches = mark_best_match (found, r, args);
      //if (found.size () > 1) cout_match_report (found, r, args);
      save_matched_read (found,tot_best_matches,r,args);
      update_counts (found, tot_best_matches, args);  //  Update counts in the db
      return;
    }
  }
  
  // no valid match found: update rejected if required
  if(args->discarded_rejected_dup>=0) { // if <0 we do not use args->rejected
    if(args->helper_threads>1)     
      xpthread_mutex_lock(&args->thread_info->mutex_rejected,__LINE__,__FILE__);
    bool first_time = args->rejected.emplace(r->dna).second;
    assert(first_time==true || args->helper_threads>1);
    if(args->helper_threads>1)     
      xpthread_mutex_unlock(&args->thread_info->mutex_rejected,__LINE__,__FILE__);
  }
  save_unmatched_read(r,args, Micro_not_found); // Update stats + save if required
}


// process the reads in args.readsFileName
//   init the agrep-mask associated to the adapter
//   for each read call the function process_single_read
void process_reads(Args &args)
{
  // If trimming is disabled it is:
  //  args.trim_adapter == false  args.safe_adapter == "" args.safe_adapter_length == 0
  if (args.trim_adapter) {  // false if user set --no-trim-adapter
    cerr << "Searching in reads for adapter " << args.safe_adapter;
    cerr << " with at most " << args.adapter_inread_distance << " errors\n";
    // init agrep mask for the adapter
    Agrep::init_mask(args.safe_adapter.c_str(),args.safe_adapter_length, args.adapter_mask);
  }
   
  singleRead r;
  // open reads file via filemanager
  filemanager *f = new filemanager(args.readsFileName);
  f->remapcase (UPPERCASE); //  sequences returned from next_seq () will always be uppercase
  f->remaptoDNA ();         //  sequences returned from next_seq () will always be DNA
  if(f->fail ()) exit(EXIT_FAIL); //  Filemanager constructor displaied the error message
  // open output files 
  args.open_outfiles();
  
  // init pthread-related data structures and create threads 
  th_params *p=NULL;
  pthread_t *t=NULL;
  if(args.helper_threads>0) {
    // init pc_buffer as PC_buffer_ratio times the number of threads
    args.thread_info->pc_buffer_init(PC_buffer_ratio*args.helper_threads,sizeof(r));
    // prepare arrays, one entry per thread
    p = new th_params[args.helper_threads];
    t = args.thread_info->thread_id;
    // create threads
    for(int i=0;i<args.helper_threads;i++) {
      p[i].id = i;
      p[i].td = args.thread_info;
      p[i].args = &args;
      xpthread_create(&t[i],NULL,consumer_process_single_read,&p[i],__LINE__,__FILE__);
    }
  }

  // ready to process reads: main loop 
  long tot_reads = 0;
  struct sequence_t *s = NULL;
  while ((s = f->next_seq (s)) != NULL) {
    assert(s->sequence!=NULL);
    ++tot_reads;
    // copy header, dna and quality score in three new C-strings;
    r.header = strdup(s->label->c_str ());
    if (s->quality == NULL) r.quality = NULL;
    else r.quality = strdup(s->quality);
    r.dna = strdup(s->sequence);
    //  check if the read quality is in Base33 or base64 alphabet
    (s->alphabet == BASE33_QUAL) ? args.base33qual = true : args.base33qual = false;
    // read is ready: write to pc_buffer or, if no helper threads, process locally  
    if(args.helper_threads>0)   
      args.thread_info->pc_buffer_write(&r);
    else {
      process_single_read(&r,&args);
      free (r.header); free(r.dna); if(r.quality) free(r.quality);  
      if(args.verbose>0 && tot_reads%1000000==0)
        fprintf(stderr,"[%jd reads] %jd seconds\n", (intmax_t)tot_reads, (intmax_t)(time(NULL)-args.start_time));
    }
    // read analysis completed: exit if desired number of reads reached
    if(tot_reads==args.max_reads) break;
  }

  // terminate consumer threads
  if(args.helper_threads>0) {
    // send illegal read to stop consumers 
    r.dna = r.quality = r.header = NULL;
    for(int i=0;i<args.helper_threads;i++) 
      args.thread_info->pc_buffer_write(&r);
    // collect tot_reads/tot_matches from threads
    long tot_processed = 0;
    for(int i=0;i<args.helper_threads;i++) {
      xpthread_join(t[i],NULL,__LINE__,__FILE__);
      tot_processed += p[i].tot_reads;
    }
    delete[] p;
    args.thread_info->pc_buffer_destroy(true);
    if(tot_processed!=tot_reads) 
      cerr << "Error: " << tot_processed <<" reads processed by threads out of " << tot_reads << endl; 
  }

  // close output files
  args.close_outfiles();
  // close reads file and free memory
  delete f;
  return;
}


// --- exact search using the trie(s) to get data for guessing the adapter

// called on each read for updating the adapter stats 
// currently this is strictly a single thread function   
bool get_adapter_stats_aux(char *r_seq, Args *args, long *tot_matches)
{
  int found=0, start=0, len=0;
  bool more_reads = true;

  // ---------------- exact search on plain trie   
  TrieNode *root = args->trie_root;
  pair<int,int> res = root->search(r_seq);
  if(res.first>=0) {                        // res.first is id of seq 
    len = args->db[res.first]->sequence.size();   // length of the match 
    start = res.second - len +1;            // starting position of match
    found++; 
  }
  // search in the reverse complement trie if requested 
  if(args->revcompl) {
    TrieNode *root_rc = args->trie_rc_root;
    res = root_rc->search(r_seq);
    if(res.first>=0) {
      len = args->db[res.first]->sequence.size();   // length of the match 
      start = res.second - len +1;            // starting position of match 
      found++;
    }
  }
  // if there was exactly one match update adapter stats
  if(found==1) {
    (*tot_matches)++;
    string dna(r_seq); // not nice, it would be better to just pass the char *pointer   
    more_reads = args->myadapter->spsav_insert (&dna, start+len);   // if spsav_insert returns false we no longer need to process reads
  }
  return more_reads; 
}


// process the reads in args.readsFileName
// for each read we call the function get_adapter_stats_aux
// currently multithread is not supported
bool get_adapter_stats(Args &args)
{
  char *dna;
  // open reads file via filemanager
  filemanager *f = new filemanager(args.readsFileName);
  f->remapcase (UPPERCASE); //  sequences returned from next_seq () will always be uppercase
  f->remaptoDNA ();         //  sequences returned from next_seq () will always be DNA
  if(f->fail ()) exit(EXIT_FAIL); //  Filemanager constructor displaied the error message
  bool more_reads  = true;   
  // ready to process reads: main loop 
  long tot_reads = 0, tot_matches = 0;
  struct sequence_t *s = NULL;
  while ((s = f->next_seq (s)) != NULL) {
    assert(s->sequence!=NULL);
    ++tot_reads;
    // at this stage we do not need name and quality, so only copy dna sequence 
    dna = strdup(s->sequence);
    more_reads = get_adapter_stats_aux(dna,&args,&tot_matches);
    free (dna); 
    if(args.verbose>0 && tot_reads%1000000==0)
      fprintf(stderr,"[%jd reads] %jd seconds\n", (intmax_t)tot_reads, (intmax_t)(time(NULL)-args.start_time));
    // read analysis completed: exit if desired number of reads reached
    if(!more_reads || tot_reads==args.max_reads) break;
  }
  // report statistics
  cerr << tot_reads << " reads processed\n";
  cerr << tot_matches << " exact matches (" << setprecision(2);
  cerr << (100.0*tot_matches)/tot_reads << "%)\n";
  // close reads file and free memory
  delete f;
  return more_reads;
}

// ----------- overloading << for vectors
template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
  if ( !v.empty() ) {
    out << '[';
    for(auto i: v) out << i << " "; 
    out << "\b]";
  }
  return out;
}
