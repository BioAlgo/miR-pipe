CC = g++ 

UNAME_M=$(shell uname -m)

#CFLAGS = -O3 -Wall -std=c99
CXXFLAGS = -O3 -Wall -pedantic -std=c++11 -g
# add this option for raspberryPI compilation
ifeq ('arm', $(UNAME_M))
CXXFLAGS += -latomic
endif

LIB = $(shell find lib  -name "*.cpp")
LIB_OBJ = $(LIB:.cpp=.o) 
OBJECTS=$(LIB_OBJ) 
EXECS=ezcount

all: .depend $(OBJECTS) $(EXECS)

# main executable 
ezcount: micro.o $(LIB_OBJ)
	$(CC) $(CXXFLAGS) $(LIB_OBJ) $< -pthread -lz -o $@  

micro.o: micro.cpp micro.hpp micro_tools.hpp constants.hpp stringTools.hpp
	$(CC) $(CXXFLAGS) -c -o $@ $<

.PHONY: clean all install

clean:
	@-rm -f $(shell find . -type f -name "*.o") dummy.o
	@-rm -f $(shell find . -type f -name "*~") dummy~
	@-rm -f $(EXECS) .depend 

install:
	@install ezcount /usr/local/bin/
	@install man/ezcount.1 /usr/local/share/man/man1/

.depend:
	$(CC) -std=c++11 -MM $(LIB) > .depend

ifeq ($(wildcard .depend), .depend)
include .depend
endif
# DO NOT DELETE
