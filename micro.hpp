#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include <utility>

#include "constants.hpp"
#include "stringTools.hpp"
#include "lib/threads/threads.hpp"
#include "lib/adapter/adapter.hpp"
#include "lib/fileManager/fileManager.hpp"
#include "lib/mirDB/mirDB.hpp"
#include "lib/args/args.hpp"

using namespace std;

// parameters passed to the pthread main function
struct th_params {
  int id;
  int64_t tot_reads;
  ThreadData *td;
  Args *args;
};

// C-style struct for a single read: header, dna and quality scores 
struct singleRead {
  char *header;
  char *dna;
  char *quality;
  // void set_values(string a, string b);
};


static TrieNode *build_trie(Args &args, bool revcompl);
// first pass to get data for determining the adapter
bool get_adapter_stats(Args &args);
// second pass to process the reads with the adapter removed
void process_reads(Args &args);
// called by process_reads to process a single read, possibly concurrently
void process_single_read(singleRead *r, Args *args);
void *consumer_process_single_read(void *v);
// report a match found by process_single_read
void match_report(vector<HDmatch> &found, singleRead *r, Args *args);
// if required saves reads where no micro was found
void save_unmatched_read(singleRead *r, Args *args, string msg="");
