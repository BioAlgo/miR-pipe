// compilation constants that can be changed by the user 
#ifndef CONSTANTS_H
#define CONSTANTS_H


// Maximum distance used in Agrep-type algorithms
#define Max_agrep_errors 3

// Maximum edit distance used to search the adapter inside the read
// If the adapter inread distance is set to d before the search phase
// we select the shortest prefix of the full adapter which is 
// at distance at lest d+1 from all the micro sequences 
#define Max_adapter_inread_distance 3

// Max_adapter_inread_distance cannot be larger than Max_agrep_errors
#if (Max_adapter_inread_distance> Max_agrep_errors)
#undef Max_adapter_inread_distance
#define Max_adapter_inread_distance Max_agrep_errors
#endif

// message for reads unmatche because the adapter was not found  
#define Adapter_not_found_message " ADAPT"
#define Micro_not_found_message " MISS"
#define Matching_found_message " MATCH"
#define Ambiguous_matching_message " AMB"
#define Read_too_short_message " SHORT"
#define Adapter_not_found   0
#define Micro_not_found     1
#define Only_trimming       2
#define Read_too_short      3


// maximum number of initial micro chars that can be skipped
#define MAX_SKIP 2


// ratio bewtween size of producer/consumer buffer and # of threads, must be >0
#define PC_buffer_ratio 3


// ===== not easily changed without modfying the code =======

// maximum lenght of a pattern in agrep-stype algorithms
// influence the data type used to represent agrep columns and masks
// to enlarge this one has to change the data type for the columns
// and masks to uint128_t, and also some integer constants used to 
// define/change such columns and masks 
#define Max_agrep_pattern_len 63


//  ====  define here possible exit status

#define EXIT_OK    0
#define EXIT_FAIL  1

#endif  /* CONSTANTS_H */
