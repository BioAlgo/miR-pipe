#include "trie.hpp"

// initialization of static constants: they must be initialized otherwise we get a compilation error
int TrieNode::tot_nodes = 0;
// set the alphabet for the whole trie node class
int TrieNode::alpha_size = 0; 
vector<uint8_t> TrieNode::alpha;         // I just wanted to test vectors
vector<int> TrieNode::alphaMap(256,-1);  // map each symbol to its position in alpha


  // constructor: create an empty trie node 
TrieNode::TrieNode() {
  assert(alpha_size>0);
  children = new TrieNode *[alpha_size];
  for(int i=0;i<alpha_size;i++)
    children[i] = NULL;
  failure = NULL;
  key = -1;
  tot_nodes++;
}
// destructor 
TrieNode::~TrieNode() {
  delete[] children;
  tot_nodes--;
}  

// in line functions 
bool  TrieNode::isleaf() {
  for(int i=0;i<alpha_size;i++)
    if(children[i]!=NULL) return false;
  return true;
}

// return a pointer to the position in the child array corresponding to c
// throw exception if c in not in alpha[]
TrieNode **TrieNode::get_pchild(int c) {
  int i = alphaMap[c];
  if(i>=0) return  &children[i];
  throw runtime_error("Invalid symbol");
}
  
// add a string to the trie recursively
void TrieNode::add_string(const char *s, int id) {
  if(s[0]=='\0') {
    if(key>=0) throw runtime_error("Duplicate string");
    key = id; // end of string; save id 
    return;
  }
  TrieNode **pchild = get_pchild(s[0]);
  if(*pchild==NULL) *pchild = new TrieNode();
  (*pchild)->add_string(s+1, id);
}


// @class function 
void TrieNode::set_alpha(string s)
{
  assert(alpha_size==0);
  alpha_size = s.size();
  assert(alpha_size>0);
  for(int i=0;i<alpha_size;i++) {
    if( find(alpha.begin(),alpha.end(),s[i])!=alpha.end() )
      throw runtime_error("Repeated symbols in alphabet");
    alpha.push_back(s[i]);
  }
  sort(alpha.begin(),alpha.end());
  assert((int)alpha.size()==alpha_size);
  // map each symbol to its position in alpha, or -1 if they are not in alpha
  for(int i=0;i<alpha_size;i++) 
    alphaMap[alpha[i]] = i;
}

// print all strings in the trie in lexicographic order and return number of strings
int TrieNode::dump(string prefix) {
  int tot = 0;
  if(key>=0) {
    cerr << key << ": " << prefix << endl;
    tot += 1;
  }
  for(int i=0;i<alpha_size;i++) {
    if(children[i]!=NULL) {
      string s(prefix);
      s.push_back(alpha[i]);
      tot += children[i]->dump(s);
    }
  }
  return tot;
}

// recursive function used called by check()
bool TrieNode::check_ric(string prefix, map<string,int>::iterator &it)
{
  // cerr << "enter: " << prefix << endl;
  // check if a string is stored at this node
  if(key>=0) {
    if(key!=it->second || prefix!=it->first) {
      cerr << "Mismatch: <" << prefix << "," << key;
      cerr << "> vs <" <<  it->first <<","<< it->second <<">\n";
      return false;
    }
    else {
      // cerr << "Match: <" << prefix << "," << key << ">\n";
      ++it; // advance iterator
    }
  }
  for(int i=0;i<alpha_size;i++) {
    if(children[i]!=NULL) {
      string s(prefix);
      s.push_back(alpha[i]);
      if(! children[i]->check_ric(s,it))
        return false;
    }
  }
  return true;
}


// recursive function to check that a trie contains the same strings as a map 
bool TrieNode::check(map<string,int> &m)
{  
  map<string,int>::iterator it = m.begin();
  if(!check_ric("",it))
    return false;
  if(it!=m.end()) {
    cerr << "Mismatch in number of strings\n";
    return false;
  }
  return true;
}


int TrieNode::compute_suffix_links()
{  
  queue<pair<TrieNode *, int>> q;
  // init root suffix link and node queue 
  this->failure = NULL;
  for(int i=0;i<alpha_size;i++)
    if(this->children[i]!=NULL)
      q.push(pair<TrieNode *, int>(this,i));
  
  // breadth first visit 
  int tot_links = 1;
  while(q.size()>0) {
    pair<TrieNode *, int> x = q.front();
    q.pop();
    TrieNode *p = x.first;            // parent 
    int c = x.second;                 // symbol on edge
    TrieNode *n = p->children[c];     // destination node
    TrieNode *candidate = p->failure;
    while(candidate!=NULL) {
      if(candidate->children[c]!=NULL) break;
      else candidate = candidate->failure;
    }      
    n->failure = candidate==NULL ? this : candidate->children[c]; 
    tot_links++; // one more link inserted
    // add n's children to the queue
    for(int i=0;i<alpha_size;i++)
      if(n->children[i]!=NULL)
        q.push(pair<TrieNode *, int>(n,i));
  }
  return tot_links;
}

// search string s[] inside the trie 
// if an exact match is found return the pair <seq_id,match_ending_position>
// otherwise return <-1,-1>
pair<int,int> TrieNode::search(const char *s)
{
  TrieNode *cur = this;
  int i=0;
  while(s[i]!=0) {
    assert(cur!=NULL);
    int j = alphaMap[s[i]];
    // if(j<0) return pair<int,int>(-1,-1); this was wrong!
    if(j>=0 && cur->children[j]!=NULL) { // valid child with label s[i] 
      cur = cur->children[j];
      if(cur->key>=0) return pair<int,int>(cur->key,i);
      i += 1;
    }
    else {
      if(cur->failure==NULL)
        i += 1;   // root reached: discard s[i]
      else
        cur = cur->failure; // follow failure link 
    }
  }
  return pair<int,int>(-1,-1);
}
    
// count how many strings in the trie are a prefix of another string     
int TrieNode::count_prefixes()
{
  int tot = 0;
  bool has_child = false;
  for(int i=0;i<alpha_size;i++)
    if(this->children[i]!=NULL) {
      has_child = true;
      tot += this->children[i]->count_prefixes();
    }
  if(has_child && key>=0) {
    // cerr << key << " is a trie prefix\n";
    tot++;
  }
  return tot;
}
