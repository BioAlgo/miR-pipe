#ifndef TRIE_H
#define TRIE_H

#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <vector>
#include <ctime>
#include <string>
#include <queue>
#include <unordered_map>
#include <map>
#include <utility>

using namespace std;


// class to represente a classical trie data structure
// each string is associated to a non negative integer id
// no duplicate strings are allowed
// the insertion procedure allows a string be a prefix of another, 
// but the search procedure does not support this yet

class TrieNode {
  TrieNode **children;
  TrieNode *failure;
  // private functions 
  bool check_ric(string prefix, map<string,int>::iterator &it);
  
public:
  int key;
  // number of created nodes 
  static int tot_nodes;
  // alphabet info valid for all nodes 
  static int alpha_size;
  static vector<uint8_t> alpha;         // I just wanted to test vectors
  static vector<int> alphaMap;          // map each symbol to its position in alpha
  static void set_alpha(string s);

  TrieNode();
  ~TrieNode();
  bool isleaf();
  TrieNode **get_pchild(int c);
  void add_string(const char *s, int id);
  
  // prototypes
  // send to cerr all strings in the trie in lex order; return number of nodes
  int dump(string s=""); 
  // check that the strings in the trie coincides with those in the map
  bool check(map<string,int> &); 
  // compute the suffix links, return number of links
  int compute_suffix_links();
  // search if a trie string is contained in s
  pair<int,int> search(const char *s);
  // count how many strings are prefixes of another one 
  int count_prefixes();  
};


#endif
