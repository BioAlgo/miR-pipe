
#include "args.hpp"

static struct option long_options[] = {
  { "reference", required_argument, nullptr, 'r' }, //  reference genome
  { "mir-gff", required_argument, nullptr, 'g' },   //  gff mirna db
  { "mir-db", required_argument, nullptr, 'd' },    //  mirna db
  { "fastq", required_argument, nullptr, 'f' },     //  input fastq

  { "counts", required_argument, nullptr, 'c' },    //  output count file
  { "count-no-header", no_argument, nullptr, 1 },   //  don't write counts header
  { "count-add-ambiguous", no_argument, nullptr, 2 },   // add ambiguous to counts 
  { "count-hide-ambiguous", no_argument, nullptr, 3 },   // hide ambiguous column  

  { "out", required_argument, nullptr, 'o' },         //  annotated output fastq
  { "out-filter-adapter", no_argument, nullptr, 4 },  //  no reads without adapter
  { "out-filter-mismatch", no_argument, nullptr, 5 }, //  no mismatching reads
  { "out-filter-match", no_argument, nullptr, 6 },    //  no matching reads  
  { "out-filter-ambiguous", no_argument, nullptr, 7 },//  no ambiguous matching reads
  { "out-filter-short", no_argument, nullptr, 8 },    //  too short reads
  { "out-RNA", no_argument, nullptr, 'A' },           //  Use RNA alphabet for output fastq
  
  { "cwd", required_argument, nullptr, 'w' },         //  prefix of working directory

  { "force-adapter", required_argument, nullptr, 'a' }, // provide adapter sequence   
  { "no-trim-adapter", no_argument, nullptr, 9 },       //  disable adapter trimming
  { "adapter-seed-length",  required_argument, nullptr, 10 }, // adapter seed length
  { "disable-mirna-count", no_argument, nullptr, 11 },  //  disable mirna count - only trimming 
  { "trimmer", no_argument, nullptr, 'T' },  // alias for --disable-mirna-count --out-filter-short --out-filter-adapter

  { "max-reads", required_argument, nullptr, 'l' },   //  MAX number of processed reads
  { "threads", required_argument, nullptr, 't' },     //  number of threads
  { "verbose", no_argument, nullptr, 'v' },           //  verbose output
  { "reverse-complement", no_argument, nullptr, 'R' },//  match also reverse-complement
  { "forget-rejected-reads", no_argument, nullptr, 12 },  // do not save set of of rejected (trimmed) reads
  { "help", no_argument, nullptr, 'h' },               //  print help
  { "mismatch", required_argument, nullptr, 'M' },     //  max mismatches
  { "adapter-edit", required_argument, nullptr, 'E' }, //  max adapter/read edit 
  { "skipped", required_argument, nullptr, 'S' },      //  max initial skipped chars
    
  { NULL, 0, NULL, 0 }
};

int Args::extract_int_parameter (const char *arg) {
  char n[256];
  int converted = (int) strtol (arg, NULL, 10);
  snprintf (n, 256, "%d", converted);
  if (strcmp  (arg, n) != 0) return -1;
  return converted;
}





// parse command line parameters
void Args::parse_command_line (int argc, char** argv) {
  int c, opt_index = 0;
  // system default for number of threads
  int numth =  thread::hardware_concurrency(); // this is 0 if no info is available 

  while ((c = getopt_long (argc, argv,"r:g:d:f:c:o:w:Ta:l:t:vhRAM:E:S:",
         long_options, &opt_index )) != -1) {
    switch(c) {
    case 'r':  //  reference genome
      this->referenceFileName.assign (optarg); 
      break;
    case 'g':  //  gff mirna db
      this->gffFileName.assign (optarg); 
      break;
    case 'd':  //  mirna db
      this->mirnaFileName.assign (optarg);
      break;
    case 'f':  //  input fastq
      this->readsFileName.assign (optarg);
      break;
    case 'c':  //  output count file
      this->countsFileName.assign (optarg);
      break;
    case 1:    //  don't write counts header
      this->counts_header = false;
      break;
    case 2:    //  add ambiguous to counts
      this->correct_with_ambiguous = true;      
      break;
    case 3:    //  hide ambiguous column
      this->ambiguous_column = false;     
      break;
    case 'o':  //  annotated output fastq
      this->outputFileName.assign (optarg);
      break;
    case 4:    //  report reads without adapter
      this->no_adapt_report=false;  
      break;
    case 5:    //  report mismatching reads
      this->mismatch_report=false;   
      break;
    case 6:    //  report matching reads
      this->matching_report=false;   
      break;
    case 7:    //  report ambiguous matching reads
      this->ambiguous_report=false;  
      break;
    case 8:   //  too short reads
      this->tooshort_report=false;  
      break;
    case 'w':  //  prefix of working directory
      this->cwd.assign (optarg);
      if (this->cwd.back () != '/') this->cwd.push_back ('/'); // add tailing /
      break;
    case 'a':  //  provide adapter sequence
      this->adapter_sequence = optarg;
      break;
    case 9:    //  disable adapter trimming
      this->trim_adapter = false;
      break;
    case 10:    //  adapter seed length
      this->adapter_seed_length = extract_int_parameter (optarg);
      break;
    case 11:    //  disable mirna count - only trimming
      this->mirna_counting = false;
      break;
    case 'T':   //  alias for --disable-mirna-count --out-filter-short --out-filter-adapter
      this->mirna_counting = false;
      this->tooshort_report=false;  
      this->no_adapt_report=false;  
      break;
    case 'l':  //  MAX number of processed reads
      this->max_reads = extract_int_parameter (optarg);
      break;
    case 't':  //  number of threads
      numth = extract_int_parameter (optarg);
      break;
    case 'v':  //  verbose output
      this->verbose++;
      break;
    case 'R':  //  match also reverse-complement
      this->revcompl=true;
      break;
    case 12:  //  do not save set of rejected (trimmed) reads
      this->discarded_rejected_dup = -1;
      break;
    case 'h':  //  print help
      print_help (argv[0]);  //  help terminates the programm
      exit (EXIT_OK);
      break;
    case 'A':  //  Use RNA alphabet for output fastq
      this->outRNA = true;  
      break;
    case 'M':  //  max mismatches
      this->maxmism = extract_int_parameter (optarg);
      break;
    case 'E':  //  max adapter/read edit
      this->adapter_inread_distance = extract_int_parameter (optarg);
      break;
    case 'S':  //  max initial skipped chars
      this->skip = extract_int_parameter (optarg);
      break;
    case '?':  //  some unexisting option was specified
      print_help (argv[0]);  //  help terminates the programm
      exit (EXIT_FAIL);      
    }
  }
  // helper threads are in addition to the main thread   
  this->helper_threads = max(0,numth-1);   
      
}

bool Args::check_command_line (void) {
  if (!this->referenceFileName.empty () && this->gffFileName.empty ()) {
    cerr << "gff database not specified extracting miRNAs from " << this->referenceFileName << endl;
    return false;
  }
  if (this->referenceFileName.empty () && !this->gffFileName.empty ()) {
    cerr << "reference file not specified extracting miRNAs from " << this->gffFileName << endl;
    return false;
  }
  if (this->gffFileName.empty () && this->mirnaFileName.empty ()) {
    cerr << "Either a reference+gff or a db of miRNAs must be specified" << endl;
    return false;
  }

  if (this->readsFileName.empty ()) {  //  Acceptable only if all the other are specified
    if (this->referenceFileName.empty () || this->gffFileName.empty () || this->mirnaFileName.empty ()) {
      if (!this->mirnaFileName.empty ()) 
        cerr << "Input fastq file not specified" << endl;
      else
        cerr << "Either -d or -f must be specified" << endl;
      return false;
    }
    else {  //  If -r -g -d are set, other options -o -c ... should not be set
      if (countsFileName.empty () == false) {  
        cerr << "No input fastq file specified with -c option" << endl;
        return false;
      }
      if (outputFileName.empty () == false) { 
        cerr << "No input fastq file specified with -o option" << endl;
        return false;
      }      
      if (mirna_counting == false) {  //  Only trimming is required
        cerr << "No input fastq file specified with --disable-mirna-count option" << endl;
        return false;
      }
    }
  }
  
  if (this->max_reads < 0) {
    cerr << "Parameter -l [--max-reads] has a wrong value" << endl;
    return false;
  }
  if (this->adapter_seed_length < 0) {
    cerr << "Parameter --adapter-seed-length has a wrong value" << endl;
    return false;
  }
  if (this->helper_threads < 0) {
    cerr << "Parameter -t [--threads] has a wrong value" << endl;
    return false;
  }
  if (this->maxmism < 0) {
    cerr << "Parameter -M, [--mismatch] has a wrong value" << endl;
    return false;
  }
  if (this->adapter_inread_distance < 0) {
    cerr << "Parameter -E, [--adapter-edit] has a wrong value" << endl;
    return false;
  }
  if (this->skip < 0) {
    cerr << "Parameter -S, [--skipped] has a wrong value" << endl;
    return false;
  }
  
  //  Capitalize the provided adapter sequence if any
  if (!this->adapter_sequence.empty ()) {
    locale loc;
    for (string::size_type i = 0; i < this->adapter_sequence.length (); i ++) {
      // Convert uppercase
      this->adapter_sequence[i] = toupper(this->adapter_sequence[i], loc);
      // Convert to DNA alphabet
      if (this->adapter_sequence[i] == 'U') this->adapter_sequence[i] = 'T';
      //  Check that the characters are all legal
      if (this->adapter_sequence[i] != 'A' && this->adapter_sequence[i] != 'C' && this->adapter_sequence[i] != 'G' && this->adapter_sequence[i] != 'T') {  //  Unecpected character in adapter sequence
  cerr << "Unexpected character in user-provided adapter sequence" << endl;
  return false;
      }
    }
    //  Check for conflicting options
    if (this->trim_adapter == false) {
      cerr << "User required not to trim adapter but an adapter sequence was specified" << endl;
      return false;
    }
    if (this->adapter_seed_length != DEFAULT_ADAPTER_SEED_LENGTH) {
      cerr << "User required not to trim adapter but an adapter seed length was specified" << endl;
      return false;
    }
  }
  if(this->adapter_inread_distance > Max_adapter_inread_distance ) {
    cerr << "Adapter in-read distance can be at most " << Max_adapter_inread_distance << endl;
    return false;
  }
   if(this->maxmism>Max_agrep_errors) {
     cerr << "Cannot handle more than " << Max_agrep_errors << " mismatches between microRNAs and read\n";
      return false;
   } 
   if(this->skip > MAX_SKIP) {
     cerr << "Cannot skip more than " << MAX_SKIP << "initial chars\n";
      return false;
   }
   if (this->adapter_seed_length != DEFAULT_ADAPTER_SEED_LENGTH) {  
      cerr << "Changing adapter seed length is not recommended unless you exactly know what you are doing. This message is only an advice" << endl;
      return true;  //  Just a warning, execution can proceed
   }
   if (this->mirna_counting == false) {  //  Only trimming is required
     if (this->trim_adapter == false) {
       cerr << "Conflicting options --disable-mirna-count and --no-trim-adapter result in notting to do\n";
       return false;
     }
     if (this->outputFileName.empty ()) {
  cerr << "No output fastq file specified with --disable-mirna-count option" << endl;
  return false;
     }
     if (!this->countsFileName.empty ()) {
  cerr << "Conflicting options --disable-mirna-count and -c [--counts]" << endl;
  return false;
     }
     //  Non-blocking warnings
     if (!this->mismatch_report)
       cerr << "Option --out-filter-mismatch ignored when --disable-mirna-count is set" << endl;
     if (!this->matching_report)
       cerr << "Option --out-filter-match ignored when --disable-mirna-count is set" << endl;
     if (!this->ambiguous_report)
       cerr << "Option --out-filter-ambiguous ignored when --disable-mirna-count is set" << endl;
     if (!this->counts_header)
       cerr << "Option --count-no-header ignored when --disable-mirna-count is set" << endl;
     if (this->correct_with_ambiguous)
       cerr << "Option --count-add-ambiguous ignored when --disable-mirna-count is set" << endl;
     if (!this->ambiguous_column)
       cerr << "Option --count-hide-ambiguous ignored when --disable-mirna-count is set" << endl;
   }

   if (this->outputFileName.empty ()) { //  Non-bloccking warnings when output fastq is not required
     if (!this->mismatch_report)
       cerr << "Option --out-filter-mismatch ignored when -o [--out] is not set" << endl;
     if (!this->matching_report)
       cerr << "Option --out-filter-match ignored when -o [--out] is not set" << endl;
     if (!this->ambiguous_report)
       cerr << "Option --out-filter-ambiguous ignored when -o [--out] is not set" << endl;
     if (!this->no_adapt_report)
       cerr << "Option --out-filter-adapter ignored when -o [--out] is not set" << endl;
     if (!this->tooshort_report)
       cerr << "Option --out-filter-short ignored when -o [--out] is not set" << endl;
     if (this->outRNA)
       cerr << "Option -A [--out-RNA] when --out [-o] is not set" << endl;
   }
   return true;
}


// parse command line parameters
// don't know why we are using "this" here 
Args::Args(int argc, char** argv) {
  parse_command_line (argc, argv);
  if (check_command_line () == false) {
    print_help (argv[0]);  //  Something in the parameters was wrong
    exit (EXIT_FAIL);
  }    
  if (!this->cwd.empty ()) {  //  Add the cwd to all file paths
    if (!this->mirnaFileName.empty ()) this->mirnaFileName.insert (0, cwd);
    if (!this->readsFileName.empty ()) this->readsFileName.insert (0, cwd);
    if (!this->outputFileName.empty ()) this->outputFileName.insert (0, cwd);
    if (!this->referenceFileName.empty ()) this->referenceFileName.insert (0, cwd);
    if (!this->gffFileName.empty ()) this->gffFileName.insert (0, cwd);
    if (!this->countsFileName.empty ()) this->countsFileName.insert (0, cwd);
  }
  if (this->helper_threads > 0)
     this->thread_info = new ThreadData(this->helper_threads,this->verbose); // # helper threads    
  if (this->trim_adapter)   // this is false if user specified --no-trim-adapter
    this->myadapter = new adapter(this->adapter_seed_length); // init adapter data structure

  // init timer
  this->start_time = time(NULL);
  //  Initialize statistics variables
  this->init_statistics ();
}


// program usage help
void Args::print_help(char *name) {
  cout << "Usage: " << endl;
  cout << "\t" << name << " -d input.db -f reads.fq(.gz) [options]" << endl;
  cout << "  or in database creation mode:" << endl;
  cout << "\t" << name << " -r reference.fa(.gz) -g mirna.gff -d output.db [options]" << endl;
  cout << "  Options:" << endl;
  cout << left << "    -r" << setw (26) << "  --reference" << "   reference genome filename (.gz accepted)" << endl;
  cout << left << "    -g" << setw (26) << "  --mir-gff" << "   gff mirna db filename" << endl;
  cout << left << "    -d" << setw (26) << "  --mir-db" << "   mirna db filename" << endl;
  cout << left << "    -f" << setw (26) << "  --fastq" << "   input fastq filename (.gz accepted)" << endl;
  cout << left << "    -c" << setw (26) << "  --counts" << "   output count file (stdout if not specified)" << endl;
  cout << left << "      " << setw (26) << "  --count-no-header" << "   don't write counts header" << endl;
  cout << left << "      " << setw (26) << "  --count-add-ambiguous" << "   add ambiguous to counts" << endl;
  cout << left << "      " << setw (26) << "  --count-hide-ambiguous" << "   hide ambiguous column" << endl;
  cout << left << "      " << setw (26) << "  --disable-mirna-count" << "   miRNAs are not searched, only trim adapter" << endl;
  cout << left << "    -o" << setw (26) << "  --out" << "   annotated output file (fastq format)" << endl;
  cout << left << "      " << setw (26) << "  --out-filter-adapter" << "   Don't report reads without adapter" << endl;
  cout << left << "      " << setw (26) << "  --out-filter-mismatch" << "   Don't report mismatching reads" << endl;
  cout << left << "      " << setw (26) << "  --out-filter-match"  << "   Don't report matching reads" << endl;
  cout << left << "      " << setw (26) << "  --out-filter-ambiguous" << "   Don't report ambiguous matching reads" << endl;
  cout << left << "      " << setw (26) << "  --out-filter-short" << "   Don't report reads too short" << endl;
  cout << left << "    -A" << setw (26) << "  --out-RNA" << "   Use RNA alphabet for output reads" << endl;
  cout << left << "    -T" << setw (26) << "  --trimmer" << "   alias for --disable-mirna-count --out-filter-short --out-filter-adapter" << endl;
  cout << left << "    -w" << setw (26) << "  --cwd" << "   prefix of working directory" << endl;
  cout << left << "    -a" << setw (26) << "  --force-adapter" << "   provide adapter sequence" << endl;
  cout << left << "      " << setw (26) << "  --no-trim-adapter" << "   disable adapter trimming" << endl;
  cout << left << "      " << setw (26) << "  --adapter-seed-length" << "   adapter seed length (default=40)" << endl;
  cout << left << "    -l" << setw (26) << "  --max-reads" << "   stop after processing MAX reads" << endl;
  int default_threads = thread::hardware_concurrency(); // this is 0 if no info is available 
  default_threads = max(1, default_threads); 

  cout << left << "    -t" << setw (26) << "  --threads" << "   number of threads (default=number of cores)" << endl;
  cout << left << "    -v" << setw (26) << "  --verbose" << "   verbose output" << endl;
  cout << left << "    -R" << setw (26) << "  --reverse-complement" << "   match also reverse-complement" << endl;
  cout << left << "      " << setw (26) << "  --forget-rejected-reads" << "   forget rejected reads (save some space)" << endl;
  cout << left << "    -h" << setw (26) << "  --help" << "   print this help" << endl;
  cout << left << "    -E" << setw (26) << "  --adapter-edit" << "   max adapter/read edit (default=2, max=" << Max_adapter_inread_distance << ")" << endl;
  cout << left << "    -M" << setw (26) << "  --mismatch" << "   max mismatches (default=2, max=" << Max_agrep_errors << ")" << endl;
  cout << left << "    -S" << setw (26) << "  --skipped" << "   max initial skipped chars (default=0, max=" << MAX_SKIP << ")" << endl;
}

// open file outputFileName if required
void Args::open_outfiles()
{
  if (outputFileName.empty ()) return; // no output fastq specified
  fout.open(outputFileName);
  if(!fout.is_open()) {
    perror(__func__);
    throw new std::runtime_error("Cannot open output file " + outputFileName);
  }
}

void Args::close_outfiles()
{
  if (!outputFileName.empty ()) fout.close();
}


/*  These are used to hundle statistics report  */

void Args::init_statistics (void) {
  stats_reads = 0;      //  total number of reads
  stats_no_adapt = 0;   //  total number of reads without adapter
  stats_match = 0;      //  total number of matched reads
  stats_rc = 0;         //  subset of matched reads in reverse complement
  stats_amb = 0;        //  subset of ambiguoursly matched reads
  stats_perfect = 0;    //  subset of matched reads with hamming = 0
  stats_unmatch = 0;    //  subset of unmatched reads
  stats_bp = 0;         //  count of all the processed bps
  stats_trimmed_bp = 0; //  count of the trimmed bps
  stats_short = 0;      //  subset of read too short
}


string Args::format_with_commas (unsigned long v) {
  string fv = to_string (v);
  for (int i = fv.length () - 3; i > 0; i -= 3) fv.insert (i, ",");
  return fv;
}
         
void Args::display_statistics (void) {
  cerr << fixed << setprecision(2);
  if (stats_reads == 0) return;   //  This is the case of some missing input file
  cerr << "==== Summary" << endl;
  cerr << setw (15) << format_with_commas (stats_bp) << " overall processed bps" << endl;
  if (trim_adapter)  //  Don't display this if no adapter trimming was required
    cerr << setw (15) << format_with_commas (stats_trimmed_bp) << " trimmed bps"<< endl;

  cerr << setw (15) << format_with_commas (stats_reads);
  cerr << " overall processed reads" << endl;
  if (trim_adapter) {  //  Don't display this if no adapter trimming was required
    cerr << setw (15) << format_with_commas (stats_no_adapt);
    if ((100.0 * stats_no_adapt) / stats_reads < 100) cerr << " ";
    if ((100.0 * stats_no_adapt) / stats_reads < 10) cerr << " ";
    cerr << " (" <<setprecision(2) << (100.0 * stats_no_adapt) / stats_reads << "%)";
    cerr << " reads seems not to have an adapter" << endl;
  }

  cerr << setw (15) << format_with_commas (stats_short);
  if ((100.0 * stats_short) / stats_reads < 100) cerr << " ";
  if ((100.0 * stats_short) / stats_reads < 10) cerr << " ";
  cerr << " (" <<setprecision(2) << (100.0 * stats_short) / stats_reads << "%)";
  cerr << " reads are too short" << endl;

  //  mirna_counting is false if counting was disabled  - don't display the other statistics
  if (mirna_counting == false) return;  
  
  cerr << setw (15) << format_with_commas (stats_unmatch);
  if ((100.0 * stats_unmatch) / stats_reads < 100) cerr << " ";
  if ((100.0 * stats_unmatch) / stats_reads < 10) cerr << " ";
  cerr << " (" <<setprecision(2) << (100.0 * stats_unmatch) / stats_reads << "%)";
  cerr << " reads seems not to match any miRNA" << endl;

  // report the number of reads discarded without checking
  if(this->discarded_rejected_dup>=0) { 
    int stats_dup = this->discarded_rejected_dup;
    cerr << setw (15) << format_with_commas(stats_dup);
    if ((100.0 * stats_dup) / stats_reads < 100) cerr << " ";
    if ((100.0 * stats_dup) / stats_reads < 10) cerr << " ";
    cerr << " (" <<setprecision(2) << (100.0 * stats_dup) / stats_reads << "%)";
    cerr << " reads were duplicates of rejected reads" << endl;
  }
  
  cerr << setw (15) << format_with_commas (stats_match);
  if ((100.0 * stats_match) / stats_reads < 100) cerr << " ";
  if ((100.0 * stats_match) / stats_reads < 10) cerr << " ";
  cerr << " (" <<setprecision(2) << (100.0 * stats_match) / stats_reads << "%)";
  cerr << " overall matching reads" << endl;

  cerr << setw (15) << format_with_commas (stats_rc);
  if ((100.0 * stats_rc) / stats_reads < 100) cerr << " ";
  if ((100.0 * stats_rc) / stats_reads < 10) cerr << " ";
  cerr << " (" << setprecision(2) << (100.0 * stats_rc) / stats_reads << "%)";
  cerr << " reads matched in reverse complement" << endl;

  cerr << setw (15) << format_with_commas (stats_amb);
  if ((100.0 * stats_amb) / stats_reads < 100) cerr << " ";
  if ((100.0 * stats_amb) / stats_reads < 10) cerr << " ";
  cerr << " (" <<setprecision(2) << (100.0 * stats_amb) / stats_reads << "%)";
  cerr << " reads matched more than one miRNA" << endl;

  cerr << setw (15) << format_with_commas (stats_perfect);
  if ((100.0 * stats_perfect) / stats_reads < 100) cerr << " ";
  if ((100.0 * stats_perfect) / stats_reads < 10) cerr << " ";
  cerr << " (" << setprecision(2) << (100.0 * stats_perfect) / stats_reads << "%)";
  cerr << " reads matched with no gaps" << endl;

}

Args::~Args() {
  // destroy adapter data structure 
  if (trim_adapter)     // this is false if user specified --no-trim-adapter
    delete myadapter;
  else 
    assert(myadapter==NULL);
  // Destroy helper threads
  if(helper_threads>0)
    delete thread_info;
  else
    assert(thread_info==NULL);
  // TODO: deallocate trie
  cerr << "Done\n"; 
}
