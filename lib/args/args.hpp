#ifndef ARGS_H
#define ARGS_H

#include <atomic>
#include <string>
#include <iomanip>
#include <unistd.h>
#include <getopt.h>
#include <cstdlib>
#include <locale>
#include <thread>
#include <unordered_set>

#include "../mirDB/mirDB.hpp"
#include "../adapter/adapter.hpp"
#include "../threads/threads.hpp"
#include "../../constants.hpp"
#include "../trie/trie.hpp"

using namespace std;

// -------------------------------------------------------------
// struct containing command line parameters and other globals
struct Args {

   string mirnaFileName;             // file containing micro descriptions
   string readsFileName;             // read file in fastq format
   string outputFileName;            // output annotated reads in fastq format
   string referenceFileName;         // reference genome fasta or fasta.gz
   string gffFileName;               // gff file with miRNA description
   string countsFileName;            // output file with counts - stdout if not specifyed
   string cwd;                       // path of current working dir to help user with command line

   string alphabet ="ACGT";
   int adapter_seed_length = DEFAULT_ADAPTER_SEED_LENGTH;     // default initial seed length for adapter
   int adapter_inread_distance = 2;  // default maximum edit distance between adapter and reads 
   int maxmism = 2;                  // default maximum number of mismatches between micro and reads
   int skip = 0;                     // default leading micro chars that can be skipped
   size_t min_read_length = 1;       // default minimal length of (trimmed) read before searching
   mirDB db;                         // full database of miRNA sequences
   vector<int> minimal_ids;          // list of minimal miRNA ids
   map<string,int> seq_id;           // map seq->id stored in the trie
   TrieNode *trie_root;              // root of the trie containing the sequences
   TrieNode *trie_rc_root;           // root of the trie containing the rev_compl sequences
   ofstream fout;                    // output file
   int verbose=0;                    // verbosity level 
   long max_reads = 0;               // maximum number of reads to process 
   bool revcompl=false;              // match the reverse complement of the string 
   time_t start_time;                // time at the start of the computation 
   adapter *myadapter=NULL;          // class for guessing the adapter 
   string adapter_sequence;          // user provided adapter sequence
   bool trim_adapter=true;           // requires to trim the adapter sequence from the reads
   string safe_adapter;              // adapter string derived from first pass 
   int safe_adapter_length;          // length of the safe adapter
   uint64_t adapter_mask[256];       // agrep mask for the adapter 
   int helper_threads;               // number of threads in addition to the main one, default is hardware_concurrency-1
   ThreadData *thread_info=NULL;     // some global info shared by all threads 
   int discarded_rejected_dup=0;     // if 0 keeps set of rejected reads to avoid to process them again, can be changed by --forget-rejected
   unordered_set<string> rejected;   // set of (trimmed) reads the algorithm rejected since it could not assign to any micro

   atomic_ulong stats_reads;         //  total number of reads
   atomic_ulong stats_no_adapt;      //  total number of reads without adapter
   atomic_ulong stats_match;         //  total number of matched reads
   atomic_ulong stats_rc;            //  subset of matched reads in reverse complement
   atomic_ulong stats_amb;           //  subset of ambiguously matched reads
   atomic_ulong stats_perfect;       //  subset of matched reads with hamming = 0
   atomic_ulong stats_unmatch;       //  subset of unmatched reads
   atomic_ulong stats_short;         //  subset of read too short
   atomic_ullong stats_bp;           //  count of all the processed bps
   atomic_ullong stats_trimmed_bp;   //  count of the trimmed bps
   bool counts_header=true;          //  Show header in count output
   bool correct_with_ambiguous=false;//  Add ambiguous matches to count column  
   bool ambiguous_column=true;       //  Show a third column with ambiguous matches
   bool no_adapt_report=true;        //  report reads without adapter
   bool mismatch_report=true;        //  report mismatching reads
   bool matching_report=true;        //  report matching reads
   bool ambiguous_report=true;       //  report ambiguous matching reads
   bool tooshort_report=true;        //  report reads too short to match any mirna
   bool mirna_counting=true;         //  disable mirna count - only trimming
   bool outRNA=false;                //  replace T with U in output reads 
   bool base33qual=true;             //  base 33 or base 64 quality alphabet (def. 33)
   int extract_int_parameter (const char *arg);
   void parse_command_line (int argc, char** argv);
   bool check_command_line (void);
   // constructor from command line parameters
   Args(int argc, char** argv);
   // show usage message
   void print_help(char *name);
   // open and close output files
   void open_outfiles();
   void close_outfiles();
   void display_statistics (void);
   void init_statistics (void);
   string format_with_commas (unsigned long v);
   // destructor 
   ~Args();
};
#endif
