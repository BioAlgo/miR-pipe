#ifndef MIRDB_H
#define MIRDB_H

#include <string>
#include <map>
#include <set>
#include <vector>
#include "../fileManager/fileManager.hpp"
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream
#include <fstream>
#include <assert.h>
#include <atomic>

using namespace std;

// struct representing a mirna sequence
// names are guaranteed to be unique and are used as keys in vectors/maps
typedef struct {
  int id;                      // unique integer ID -  Assigned on creation
  string name;                 // unique name
  string chromosome;
  int init_coord;
  int end_coord;
  char strand;                 // either + or -
  string sequence;             // actual mirna sequence in capital letters
  atomic_ulong count;             //  This will contain the miRNA counts
  atomic_ulong ambiguous_count;   //  This will contain the subset of ambiguous
  vector<string> duplicates;   // names of seqs which are identical to this
  vector<string> has_subsequence; // names of seqs contained in this
  map<string, size_t> is_subsequence_of; // names of seqs which contain this and corresponding initial position of this
  bool is_minimal;   //  Giovanni needs it - they are the micro initially searched
} mirna_t;
  
class mirDB {
  int gff_mir_col;
  string gff_mir_val;
  int gff_name_col;
  string gff_name_val;
  map<string, mirna_t*> mirna_list;
  mirna_t **mirna_id_list;
  int tot_mirna;
  size_t min_micro_length;   //  length of the shortest sequence in db
  void build_id_list (void);
  mirna_t *read_gff_line (const string *line);
  char capitalize (char nucleotide);
  char complement (char nucleotide);
  pair<int, vector<size_t> > find_sub_with_ham (string *s1, string *s2, u_int maxD=0);
 public:
  int tot_gff_lines; // Store the number of lines in gff including duplicates
  mirDB (int mir_col=3, string mir_val="miRNA", int name_col=9, string name_val="Name");
  ~mirDB (void);
  bool read_gff (const string &gff_filename);
  int read_sequences (string &fasta_filename);
  void dump (void);
  bool load (const string &filename);
  bool store (const string &filename);
  void a2adist (void);
  mirna_t *operator[] (const string s);
  mirna_t *operator[] (const int s); 
  vector<string> keys (bool only_minimal=false);
  vector<int> ids (bool only_minimal=false);
  size_t minseq_len (void);
  pair<ostream *, filebuf *> open_stream (const string &filename);
  void save_counts (const string &filename=std::string (), char sep='\t', bool header=true, bool ambiguous=true, bool correct_with_ambiguous=false);
};
#endif /* MIRDB_H */
