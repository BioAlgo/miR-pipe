
#include "mirDB.hpp"

mirna_t *mirDB::operator[] (const string s) {
  map<string,mirna_t*>::iterator it;
  it = mirna_list.find (s);  //  Search the miRNA record
  if (it == mirna_list.end ()) return NULL;  //  Not found
  else return it->second;  //  Access the miRNA structure
}

mirna_t *mirDB::operator[] (const int s) {
  if (s < tot_mirna) return mirna_id_list[s];
  else return NULL;
}

vector<string> mirDB::keys (bool only_minimal) {
  vector<string> key_list;  //  The list of miRNAs
  map<string,mirna_t*>::iterator it;
  for (it = mirna_list.begin (); it != mirna_list.end (); it ++) {
    if (only_minimal == true && it->second->is_minimal == false) continue;
    key_list.push_back (it->first);
  }
  return key_list;
}
vector<int> mirDB::ids (bool only_minimal) {
  vector<int> id_list;  //  The list of miRNAs
  map<string,mirna_t*>::iterator it;
  for (it = mirna_list.begin (); it != mirna_list.end (); it ++) {
    if (only_minimal == true && it->second->is_minimal == false) continue;
    id_list.push_back (it->second->id);
  }
  return id_list;
}
size_t mirDB::minseq_len (void) {
  // returns the length of the shortest sequence in db
  return min_micro_length;
}

mirDB::mirDB (int mir_col, string mir_val, int name_col, string name_val) {
  gff_mir_col = mir_col - 1; // From human readable number to 0 based
  gff_mir_val = mir_val;
  gff_name_col = name_col - 1;
  gff_name_val = name_val;
  tot_gff_lines = 0;   // no lines have been read yet
  mirna_id_list = NULL;
  tot_mirna = 0;
  min_micro_length = 0;
}

mirDB::~mirDB (void) {
  while (!mirna_list.empty ()) {
    mirna_t *mir_record = mirna_list.begin()->second;
    delete mir_record;
    mirna_list.erase (mirna_list.begin ());
  }
  if (mirna_id_list != NULL) delete[] mirna_id_list;
}


void mirDB::build_id_list (void) {
  map<string,mirna_t*>::iterator it;
  mirna_id_list = new mirna_t*[tot_mirna];
  for (it = mirna_list.begin (); it != mirna_list.end (); it ++)
    mirna_id_list[it->second->id] = it->second;
}

bool mirDB::load (const string &filename) { // Load miRNAs from db
  mirna_t *mir_record;
  string line;
  ifstream infile;
  infile.open (filename);
  if (infile.fail()) return false;
  while (getline (infile, line)) {
    //  Skip lines with comments -- must start with # as first character
    if (line.at (0) == '#') continue;
    stringstream line_fields (line);
    vector<string> fields;
    string field;
    while (getline (line_fields, field, ';')) { //  Split
      fields.push_back (field);
    }
    if (fields.size () != 7) {
      cerr << "Error in line " << line << endl;
      cerr << "Wrong number of elements in record, line skipped" << endl;
      continue;
    }
    mir_record = new mirna_t ();
    //  Initialize counts
    mir_record->count = 0;           //  This will contain the miRNA counts
    mir_record->ambiguous_count = 0; //  This will contain the subset of ambiguous
    //  Mirna name
    mir_record->name = fields[0];
    //  Mirna id
    mir_record->id = tot_mirna;
    tot_mirna ++;
    //  Coordinates
    mir_record->strand = fields[1].at (fields[1].length () - 1);
    mir_record->init_coord = 0;
    mir_record->end_coord = 0;
    int cf = 0;
    for (string::iterator it = fields[1].begin (); it != fields[1].end (); it ++) {
      char c = *it;
      if (*it == ':' || *it == '-' || *it == '+') {
  cf ++;
  continue;
      }
      if (cf == 0) {
  mir_record->chromosome += *it;
  continue;
      }
      assert (isdigit (c));
      int s = c - '0';
      if (cf == 1) mir_record->init_coord = (mir_record->init_coord * 10) + s;
      if (cf == 2) mir_record->end_coord = (mir_record->end_coord * 10) + s;
    }
    //  Load sequence and convert it in uppercase DNA - as it should already be
    for (string::iterator it = fields[2].begin (); it != fields[2].end (); it ++) {
      switch (*it) {
      case 'a':
      case 'A':
  mir_record->sequence.push_back ('A');
  break;
      case 'c':
      case 'C':
  mir_record->sequence.push_back ('C');
  break;
      case 'g':
      case 'G':
  mir_record->sequence.push_back ('G');
  break;
      case 't':
      case 'T':
      case 'u':
      case 'U':
  mir_record->sequence.push_back ('T');
  break;
      default:
  cerr << "Error parsing file: some information could not be loaded" << endl;
  continue;
      }
    }
    //  Update statistics on the shortest sequence in db
    if (min_micro_length == 0 || mir_record->sequence.size () < min_micro_length)
      min_micro_length = mir_record->sequence.size ();
    
    //  Duplicates
    if (fields[3].length () != 0) {
      stringstream sub_fields (fields[3]);
      while (getline (sub_fields, field, ',')) { //  Split
  mir_record->duplicates.push_back (field);
      }
    }

    //  is_subsequence_of
    if (fields[4].length () != 0) {
      stringstream sub_fields (fields[4]);
      while (getline (sub_fields, field, ',')) { //  Split
  size_t colon = field.find_first_of (':');
  if (colon == string::npos) {
    cerr << "Error parsing file: some information could not be loaded" << endl;
    continue;
  }
  string mir_name = field.substr (0, colon);
  size_t p;
  istringstream (field.substr (colon+1)) >> p;
  mir_record->is_subsequence_of.insert (std::pair<string, size_t>(mir_name, p));
      }
    }
    
    //  has_subsequence  
    if (fields[5].length () != 0) {
      stringstream sub_fields (fields[5]);
      while (getline (sub_fields, field, ',')) { //  Split
  mir_record->has_subsequence.push_back (field);
      }
    }

    //  is_minimal - Must be just a character Y/N
    if (fields[6].length () == 1) {
      switch (fields[6].at (0)) {
      case 'Y':
      case 'y':
  mir_record->is_minimal = true;
  break;
      case 'N':
      case 'n':
  mir_record->is_minimal = false;
  break;
      default:
  cerr << "Error in line " << line << endl;
  cerr << "miRNA will be exluded from counting" << endl;
  mir_record->is_minimal = false;
  break;
      }  
    }
    else {
      cerr << "Error in line " << line << endl;
      cerr << "miRNA will be exluded from counting" << endl;
      mir_record->is_minimal = false;
    }  
    
    //  Insert the record in the list 
    pair<map<string, mirna_t*>::iterator, bool> ret;
    ret = mirna_list.insert (std::pair<string,mirna_t*>(mir_record->name, mir_record));
    //  A mirna with the same name is already in the db
    if (ret.second == false) delete mir_record;
  }
  infile.close ();
  build_id_list ();
  return true;
}


bool mirDB::read_gff (const string &gff_filename) {
  mirna_t *mir_record;
  string line;
  ifstream infile;
  infile.open (gff_filename);
  if (infile.fail()) return false;
  while (getline (infile, line)) {
    mir_record = read_gff_line (&line);
    if (mir_record != NULL) {
      //  Mirna id 
      mir_record->id = tot_mirna;
      tot_mirna ++;
      //  Initialize non gff specific fields
      mir_record->count = 0;           //  This will contain the miRNA counts
      mir_record->ambiguous_count = 0; //  This will contain the subset of ambiguous
      mir_record->is_minimal = true; //  This is updated by a2adsist () if false
      tot_gff_lines ++;
      pair<map<string, mirna_t*>::iterator, bool> ret;
      ret = mirna_list.insert ( pair<string,mirna_t*>(mir_record->name, mir_record) );
      //  A mirna with the same name is already in the db
      if (ret.second == false) delete mir_record;
    }
  }
  infile.close ();
  build_id_list ();
  return true;
}

char mirDB::capitalize (char nucleotide) {
  switch (nucleotide) {
  case 'a':
  case 'A':
    return 'A';
  case 'c':
  case 'C':
    return 'C';
  case 'g':
  case 'G':
    return 'G';
  case 'T':
  case 't':
    return 'T';
  }
  return 'N';
}
char mirDB::complement (char nucleotide) {
  switch (nucleotide) {
  case 'a':
  case 'A':
    return 'T';
  case 'c':
  case 'C':
    return 'G';
  case 'g':
  case 'G':
    return 'C';
  case 'T':
  case 't':
    return 'A';
  }
  return 'N';
}

// return value: -2 miRNA out of range -1 file not found, 0 success, >0 miRNAs not initialized

int mirDB::read_sequences (string &fasta_filename) {  
  struct sequence_t *s = NULL;
  map<string,mirna_t*>::iterator mirElem;
  filemanager *f = new filemanager (fasta_filename);
  f->remapcase (UPPERCASE); //  sequences returned from next_seq () will always be uppercase
  f->remaptoDNA ();         //  sequences returned from next_seq () will always be DNA
  if (f->fail ()) return -1; //  Error message is in the constructor
  set<string> chromosomes;
  //  Collect the names of the chromosomes in the gff file
  for (map<string,mirna_t*>::iterator it=mirna_list.begin (); it!=mirna_list.end (); it ++) {
    chromosomes.insert (it->second->chromosome);
  }
  //  Iterate over the fasta file
  s = f->next_seq (s);
  while (s != NULL) {
    //  If the loaded chromosome is not among those of the gff we can skip it
    if (chromosomes.find (*s->label) != chromosomes.end ()) {
      //  Iterate over the db of miRNAs
      for (mirElem = mirna_list.begin (); mirElem != mirna_list.end (); mirElem ++) {
  mirna_t *mir = mirElem->second;
  if (mir->chromosome == *s->label) {
    //  Malformed endpoint must be higher also with strand -
    if (mir->init_coord > mir->end_coord) {
      cerr << mir->name << " ";
      return -2;
    }
    if (mir->init_coord < 1 || mir->end_coord > s->sequence_size) {   //  Out of range
      cerr << mir->name << " ";
      return -2; 
    }   
    mir->sequence = "";
    if (mir->strand == '+') {
      for (int i = mir->init_coord - 1; i < mir->end_coord; i ++) // 1-based coords
        mir->sequence += capitalize (s->sequence[i]);    //  Makes uppercase
    }
    else {   //  Inverse complement on stand -
      for (int i = mir->end_coord - 1; i >= mir->init_coord - 1; i --) // 1-based coords
        mir->sequence += complement (s->sequence[i]);  //  Makes uppercase
    }
  }
      }
    }
    s = f->next_seq (s);
  }
  //  Iterate over the db of miRNAs
  int uninitialized_miRNAs = 0;
  for (mirElem = mirna_list.begin (); mirElem != mirna_list.end (); mirElem ++) {
    mirna_t *mir = mirElem->second;
    if (mir->sequence.size () == 0) uninitialized_miRNAs ++; 
    //  Update statistics on the shortest sequence in db
    if (min_micro_length == 0 || mir->sequence.size () < min_micro_length)
      min_micro_length = mir->sequence.size ();
  }
  // free memory
  delete s;
  delete f;
  return uninitialized_miRNAs;
}

bool mirDB::store (const string &filename) {  //  Store to file
  vector<string>::iterator d;
  map<string, vector<size_t> >::iterator s;
  vector<size_t>::iterator pos;
  map<string,mirna_t*>::iterator it;
  map<string, size_t>::iterator subs;
  std::ofstream ofs (filename, std::ofstream::out);
  if (ofs.fail()) {
    cerr << "Warning: unable to write miRNA db file" << endl;
    return false;
  }
  for (it=mirna_list.begin (); it!=mirna_list.end (); it ++) {
    mirna_t *mir_record = it->second;
    ofs << mir_record->name << ";";
    ofs << mir_record->chromosome << ":" << mir_record->init_coord << "-";
    ofs << mir_record->end_coord << mir_record->strand << ";";
    ofs << mir_record->sequence << ";";

    for (d = mir_record->duplicates.begin (); d != mir_record->duplicates.end (); d ++){
      if (d != mir_record->duplicates.begin ()) ofs << "," << *d;
      else ofs << *d;
    }
    ofs << ";";

    for (subs = mir_record->is_subsequence_of.begin (); subs != mir_record->is_subsequence_of.end (); subs ++) {
      if (subs != mir_record->is_subsequence_of.begin ())
  ofs << "," << subs->first << ":" <<  subs->second;
      else
  ofs << subs->first << ":" <<  subs->second;
    }
    ofs << ";";

    for (d = mir_record->has_subsequence.begin (); d != mir_record->has_subsequence.end (); d ++){
      if (d != mir_record->has_subsequence.begin ()) ofs << "," << *d;
      else ofs << *d;
    }    
    ofs << ";";

    if (mir_record->is_minimal == true) ofs << "Y";
    else ofs << "N";

    ofs << ";" << endl;
    
  }
  ofs.close ();
  return true;
}

void mirDB::dump (void) {  //  Essentially for debugging - duplicates are already removed
  vector<string>::iterator d;
  map<string, vector<size_t> >::iterator s;
  map<string, size_t>::iterator subs;
  vector<size_t>::iterator pos;
  map<string,mirna_t*>::iterator it;
  for (it = mirna_list.begin (); it != mirna_list.end (); it ++) {
    mirna_t *mir_record = it->second;
    cout << mir_record->name << " ";
    cout << mir_record->id << " ";
    cout << mir_record->chromosome << ":";
    cout << mir_record->init_coord << "-";
    cout << mir_record->end_coord;
    cout << mir_record->strand << " ";
    cout << mir_record->sequence << " ";
    if (mir_record->is_minimal == true) cout << "Minimal ";
    else cout << "Not_min ";
    
    for (d = mir_record->duplicates.begin (); d != mir_record->duplicates.end (); d ++){
      if (d != mir_record->duplicates.begin ()) cout << "," << *d;
      else cout << *d;
    }
    cout << " ";

    for (subs = mir_record->is_subsequence_of.begin (); subs != mir_record->is_subsequence_of.end (); subs ++) {
      if (subs != mir_record->is_subsequence_of.begin ())
  cout << "|" << subs->first << ":" <<  subs->second;
      else
  cout << subs->first << ":" <<  subs->second;
    }
    cout << " ";

    for (d = mir_record->has_subsequence.begin (); d != mir_record->has_subsequence.end (); d ++){
      if (d != mir_record->has_subsequence.begin ())  cout << "," << *d;
      else cout << *d;
    }    
    cout << endl;

  }
}

mirna_t *mirDB::read_gff_line (const string *line) {
  stringstream line_fields (*line);
  vector<string> fields;
  string field;
  mirna_t *db_line = NULL;
  if (line->at (0) == '#') return NULL;

  while (getline (line_fields, field, '\t')) {
    fields.push_back (field);
  }
  if (fields[gff_mir_col] != gff_mir_val) return NULL;

  std::size_t pos = fields[gff_name_col].find (gff_name_val);  
  if (pos == string::npos) return NULL;
  db_line = new mirna_t ();
  db_line->name = fields[gff_name_col].substr (pos + gff_name_val.length () + 1);
  pos = db_line->name.find (";");  
  if (pos != string::npos) db_line->name = db_line->name.substr (0, pos);
  db_line->chromosome = fields[0];
  db_line->init_coord = stoi (fields[3]);
  db_line->end_coord = stoi (fields[4]);
  db_line->strand = fields[6].at (0);
  db_line->sequence = "";  //  Initialize with empty sequence
  return db_line;
}

pair<int, vector<size_t> > mirDB::find_sub_with_ham (string *s1, string *s2, u_int maxD) {
  pair<int, vector<size_t> > retcode;
  string *shorter = s1, *longer = s2;
  int shorter_length = shorter->length (), longer_length = longer->length ();
  if (shorter_length > longer_length) {
    std::swap (shorter_length, longer_length);
    std::swap (shorter, longer);
  }
  for (int i = 0; i <= longer_length - shorter_length; i ++) {
    retcode.second.clear ();
    for (int j = 0; j < shorter_length && retcode.second.size () <= maxD; j ++) {
      if (longer->at (i+j) != shorter->at (j)) retcode.second.push_back (i+j);
    }
    if (retcode.second.size () <= maxD) {
      retcode.first = i;
      return retcode;
    }
  }
  retcode.first = -1;
  return retcode;
}

//  Compute all aganinst all hamming to find "similar" elements
void mirDB::a2adist (void) {
  map<string,mirna_t*>::iterator i, j;
  vector<size_t> d;
  mirna_t *mir_record;
  int mir_length;
  pair<int, vector<size_t> > f;

  for (i = mirna_list.begin (); i != mirna_list.end (); i ++) {
    mir_record = i->second;
    mir_length = mir_record->sequence.length ();
    for (j = mirna_list.begin (); j != i; j ++) {
      int other_length = j->second->sequence.length ();
      f = find_sub_with_ham (&mir_record->sequence, &j->second->sequence);
      // f is something like <offset_of_longer, vector<errpos1, errpos2, ...> >
      if (f.first == -1) continue; //  Neither subsequence nor duplicate
      //cout << mir_record->name << " " << mir_record->sequence << " " << j->second->name << " " << j->second->sequence << " " << f.first << " " << f.second.size () << endl;
      assert (f.second.size () == 0);
      if (mir_length == other_length) {  //  duplicates
  mir_record->duplicates.push_back (j->second->name);
  j->second->duplicates.push_back (mir_record->name);
  j->second->is_minimal = false;  //  Set one of the two as not minimal
  continue;
      }
      //  Subsequence
      if (mir_length < other_length) {
  mir_record->is_subsequence_of.insert ( std::pair<string, size_t >(j->second->name, f.first) );    
  j->second->has_subsequence.push_back (mir_record->name);
  j->second->is_minimal = false;  //  Having a subsequence it can't be minimal  
      }
      else {
  j->second->is_subsequence_of.insert ( std::pair<string, size_t >(mir_record->name, f.first) );  
  mir_record->has_subsequence.push_back (j->second->name);
  mir_record->is_minimal = false;  //  Having a subsequence it can't be minimal
      }
    } // for j
  }  // for i
}

pair<ostream *, filebuf *> mirDB::open_stream (const string &filename) {
  filebuf *fb = new filebuf ();
  pair<ostream *, filebuf *> retval;
  if (filename.empty ()) {
    retval.first = &cout;
    retval.second = fb;
    return retval;
  }
  fb->open (filename, std::ios::out);
  ostream *os = new ostream (fb);
  retval.first = os;
  retval.second = fb;
  return retval;
}

void mirDB::save_counts (const string &filename, char sep, bool header, bool ambiguous, bool correct_with_ambiguous) {
  pair<ostream *, filebuf *> os = open_stream (filename);  // If empty write on stdout
  ostream *s = os.first;
  map<string,mirna_t*>::iterator i;
  mirna_t *mir_record;
  if (s->fail ()) {
    cerr << "Error: unable to write count file" << endl;
    return;
  }
  if (header) {  //  print two or threee column output
    if (ambiguous) *s << "miRNA" << sep << "count" << sep << "ambiguous" << endl;
    else *s << "miRNA" << sep << "count" << endl;
  }
  for (i = mirna_list.begin (); i != mirna_list.end (); i ++) {
    mir_record = i->second;
    unsigned int tot = mir_record->count;
    //  Add the contribution of ambiguous reads which is half of their value because
    //  they have been added to at least two mirnas
    if (correct_with_ambiguous) tot += mir_record->ambiguous_count; 
    *s << mir_record->name << sep << tot;
    if (ambiguous) *s << sep << mir_record->ambiguous_count; // write ambiguous reads
    *s << endl;
  }
  if (!filename.empty ()) os.second->close ();  //  Close stream if it is a file
}
