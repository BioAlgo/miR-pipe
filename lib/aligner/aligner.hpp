#ifndef ALIGNER_H
#define ALIGNER_H

#include <string>
#include <iostream>
#include <algorithm>
#include <array>
#include <vector>

#define S1_GLOBAL_TAIL 1
#define S2_GLOBAL_TAIL 2
#define LOCAL_ALIGNMENT 4
#define S1_GLOBAL_HEAD 8
#define S2_GLOBAL_HEAD 16

#define GLOBAL_HEAD S1_GLOBAL_HEAD | S2_GLOBAL_HEAD
#define GLOBAL_TAIL S1_GLOBAL_TAIL | S2_GLOBAL_TAIL
#define S1_GLOBAL S1_GLOBAL_HEAD | S1_GLOBAL_TAIL
#define S2_GLOBAL S2_GLOBAL_HEAD | S2_GLOBAL_TAIL
#define GLOBAL_ALIGNMENT S1_GLOBAL | S2_GLOBAL

#define MAX_STR_LENGTH 40
#define LEFT_GAP 0
#define UP_GAP 1
#define MATCH 2
#define MISMATCH 3
#define UNINITILIALIZED -1
using namespace std;

class aligner {
  string *sequences[2];
  int m, n;
  array<int16_t, (MAX_STR_LENGTH + 1) * (MAX_STR_LENGTH + 1)> M; 
  int16_t match_score;
  int16_t mismatch_score;
  int16_t gap_score;
  int findOptimalAlignmentPosition (char flags=0);
public:
  ~aligner (void);
  aligner (int16_t _match_score=1, int16_t _mismatch_score=-1, int16_t _gap_score=-3);
  void fill_matrix (string *s1, string *s2, char flags=0);
  pair<string, int16_t> buildAlignment (char flags=0);
  void dump (void);
  vector<int16_t> *copy (void);  //  Make a copy of M in a tight vector for external use
};

#endif /* ALIGNER_H */
