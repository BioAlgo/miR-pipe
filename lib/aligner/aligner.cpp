#include "aligner.hpp"


aligner::aligner (int16_t _match_score, int16_t _mismatch_score, int16_t _gap_score) {
  match_score = _match_score;
  mismatch_score = _mismatch_score;
  gap_score = _gap_score; 
}


void aligner::fill_matrix (string *s1, string *s2, char flags) {
  int16_t g1, g2, mm, highest_val;
  sequences[0] = s1;
  sequences[1] = s2;
  //  By design the alignment can involve at most the first 40 characters
  m = (sequences[0]->length () < MAX_STR_LENGTH) ? sequences[0]->length () + 1 : MAX_STR_LENGTH + 1;
  n = (sequences[1]->length () < MAX_STR_LENGTH) ? sequences[1]->length () + 1 : MAX_STR_LENGTH + 1;  
  for (int i = 0; i < (MAX_STR_LENGTH + 1) * (MAX_STR_LENGTH + 1); i ++) M[i] = 0;

  //  Allows global alignment either on s1, s2 or both
  if ((flags & S1_GLOBAL_HEAD) != 0)
    for (int i = 1; i < m; i ++) M[i] = i * gap_score;
  if ((flags & S2_GLOBAL_HEAD) != 0)
    for (int j = 1; j < n; j ++) M[(j * m)] = j * gap_score;

  //[j][i] ===>  (j * m) + i
  //[j][i-1] ===>  (j * m) + i - 1
  //[j-1][i] ===> ((j-1) * m) + i
  //[j-1][i-1] ===> ((j-1) * m) + i - 1
  for (int j = 1; j < n; j ++) {
    for (int i = 1; i < m; i ++) {
      g1 = M[((j-1) * m) + i] + gap_score;
      g2 = M[(j * m) + i - 1] + gap_score;
      if (sequences[0]->at (i-1) == sequences[1]->at (j-1)) {
	mm = M[((j-1) * m) + i - 1] + match_score;
      }
      else {
	mm = M[((j-1) * m) + i - 1] + mismatch_score;
      }
      highest_val = g1;
      if (g2 > highest_val) highest_val = g2;
      if (mm > highest_val) highest_val = mm;
      //  Used for local alignments to keep the matrix non negative
      if ((flags & LOCAL_ALIGNMENT) != 0 && highest_val < 0) highest_val = 0;
      M[(j * m) + i] = highest_val;
    }
  }
}

int aligner::findOptimalAlignmentPosition (char flags) {
  int i, j;
  int maxval, bestPosition;

  if ((flags & LOCAL_ALIGNMENT) != 0) { //  Local alignment
    maxval = M[0];
    bestPosition = 0;
    for (i = 1; i < m * n; i ++) {
      if (M[i] > maxval) {
	maxval = M[i];
	bestPosition = i;
      }
    }
    return bestPosition;
  }
  
  maxval = M[(m * n) - 1];     //  Global
  bestPosition = (m * n) - 1;  //  Global
  if ((flags & S2_GLOBAL_TAIL) == 0) { //  Semiglobal on S2 
    /* search the highest value in the right border of the matrix  */
    for (j = 1; j < n; j ++) {
      if (M[(j * m) - 1] > maxval) {
        bestPosition = (j * m) - 1;
        maxval = M[(j * m) - 1];
      }
    }
  }
  if ((flags & S1_GLOBAL_TAIL) == 0) { //  Semiglobal on S1   
    /* search the highest value in the bottom of the matrix  */
    for (i = 0; i < m; i ++) {
      if (M[((n - 1) * m) + i] > maxval) {
        bestPosition = ((n - 1) * m) + i;
        maxval = M[((n - 1) * m) + i];
      }
    }
  }
  return bestPosition;
}

aligner::~aligner (void) {
}

pair<string, int16_t> aligner::buildAlignment (char flags) {
  int i,j, pos, op;
  pair<string, int16_t> align_s;
  bool localStop = false;
  pos = findOptimalAlignmentPosition (flags);
  align_s.second = M[pos];
  
  i = pos / m;
  j = pos % m;
  while (j != 0 && i != 0 && localStop == false) {
    pos = (i * m) + j;
    op = UNINITILIALIZED;
    if (M[pos] == M[pos - 1] + gap_score) op = LEFT_GAP;
    if (M[pos] == M[pos - m] + gap_score) op = UP_GAP;
    if (sequences[0]->at(j-1) == sequences[1]->at(i-1)) {
      if (M[pos] == M[pos - m - 1] + match_score)
        op = MATCH;
    }
    else {
      if (M[pos] == M[pos - m - 1] + mismatch_score)
        op = MISMATCH;
    }
    switch (op) {
    case MATCH:
      align_s.first.push_back ('=');
      j --;
      i --;
      break;
    case MISMATCH:
      align_s.first.push_back ('!');
      j --;
      i --;
      break;
    case UP_GAP:
      align_s.first.push_back ('2');
      i --;
      break;
    case LEFT_GAP:
      align_s.first.push_back ('1');
      j --;
      break;
    case UNINITILIALIZED:  // This must never happen - in case the program must fail
      align_s.first.clear ();
      align_s.second = -1;
      return align_s;
    }
    //  Local alignment stops when reackes 0
    if (((flags & LOCAL_ALIGNMENT) != 0) && (M[(i * m) + j] == 0)) localStop = true;
  }
  
  if ((flags & S1_GLOBAL_HEAD) != 0) 
    for (; j > 0; j --) align_s.first.push_back ('1');

  if ((flags & S2_GLOBAL_HEAD) != 0) 
    for (; i > 0; i --) align_s.first.push_back ('2');

  reverse (align_s.first.begin (), align_s.first.end ());
  return align_s;
}

void aligner::dump (void) {
  int i, j;
  for (i = 0; i < m; i ++) {
    if (i == 0) cout << "- - ";
    else cout << sequences[0]->at(i-1) << " ";
  }
  cout << endl;
  for (j = 0; j < n; j ++) {
    if (j == 0) cout << "- ";
    else cout << sequences[1]->at(j-1) << " ";
    for (i = 0; i < m; i ++)
      cout << M[(j * m) + i] << " ";
    cout << endl;
  }
}

vector<int16_t> *aligner::copy (void) {
  vector<int16_t> *c = new vector<int16_t> (m  * n);
  for (int i = 0; i < m * n; i ++) c->at (i) = M[i];
  return c;
}
