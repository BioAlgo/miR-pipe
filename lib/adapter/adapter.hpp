#ifndef ADAPTER_H
#define ADAPTER_H

#include <vector>
#include <array>
#include <string>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <bitset>
#include "../aligner/aligner.hpp"   

#include <tuple> //  Only for adapter_helper
#include <set>  //  Only for adapter_helper
#include "../mirDB/mirDB.hpp"  //  Only for adapter_helper

#define SPACE_SAVING_LENGTH 1000
#define SPACE_SAVING_HISTORY 1000
#define MAX_PREFIX_GAPS 3
#define MAX_PREFIX_SHIFT 10
#define DEFAULT_ADAPTER_SEED_LENGTH 40

using namespace std;

typedef struct {
  string sequence;   //  Should be 45 characters
  array<u_int64_t, MAX_PREFIX_GAPS> codes; //  payload
  unsigned int count;
  unsigned int epsilon;
  int alignment_offset;
} space_saving_t;

class adapter {
  vector<array<int, 5> > adapt_matrix;
  array<space_saving_t, SPACE_SAVING_LENGTH> spsav;  // space saving data structure
  bitset<SPACE_SAVING_HISTORY> spsav_history;
  int tot_seen;  //  Number of elements seen so far building the space saving structure
  int spsav_next_free;  //  index of the first free elem in spsav
  int maxlength;
  int adapter_seed_length;
  float adapter_spsav_accuracy;
  float adapter_signal_breakdown_threshold;
  int supporting_sequences;
  int random_characters;
  void spsav_init (void);
  void reset (void);
  int hash (char nucleotide);
  u_int64_t hash_sequence (string *sequence, int offset, int length);  
  int spsav_search (string *sequence, int init_position);
  void compute_adapter_alignments (void);
  string get_adapter_sequence (void);
public:
  adapter (int seed_length = DEFAULT_ADAPTER_SEED_LENGTH, float spsav_accuracy = 0.95, float signal_breakdown_threshold = 0.8);
  ~adapter (void);
  bool spsav_insert (string *sequence, int init_position = 0);
  void spsav_dump (void);
  string guess_adapter (void);
  void dump (int verbose_level=1);
  // adapter_helper function that conceptually should not be part of this class
  tuple<int, int, string> _adapter_min_length (mirDB &db, string &adapter, int tot_errors);
};

#endif /* ADAPTER_H */
