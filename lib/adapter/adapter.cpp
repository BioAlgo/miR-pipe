#include "adapter.hpp"

adapter::adapter (int seed_length, float spsav_accuracy, float signal_breakdown_threshold) {
  adapter_seed_length = seed_length;
  adapter_spsav_accuracy = spsav_accuracy;
  adapter_signal_breakdown_threshold = signal_breakdown_threshold;
  maxlength = 0;
  supporting_sequences = 0;
  random_characters = 0;
  spsav_history.set (); // Count of the recent events that modified the space saving
  tot_seen = 0;
  /*  Space saving data structure initialization  */
  spsav_init ();
}

adapter::~adapter (void) {
}

void adapter::spsav_init (void) {
  /*  Space saving data structure initialization  */
  spsav_next_free = 0;
  for (int i = 0; i < SPACE_SAVING_LENGTH; i ++) {
    spsav[i].count = 0;
    spsav[i].epsilon = 0;
    for (int j = 0; j < MAX_PREFIX_GAPS; j ++) spsav[i].codes[j] = 0;
    spsav[i].sequence = "";
    spsav[i].alignment_offset = 0;
  }
}

void adapter::reset (void) {
  spsav_history.set (); // Count of the recent events that modified the space saving
  tot_seen = 0;
  spsav_init ();
  adapter_seed_length -= 10;
  if (adapter_seed_length > 0)
    cerr << "Warning: auto-tuning seed length for adapter identification " << adapter_seed_length << endl;
  else
    cerr << "Error: impossible to find seed length for adapter identification " << adapter_seed_length << endl;
}

int adapter::hash (char nucleotide) {  // ACGTN or acgtn
  int code;
#if defined (__arm__) 
  asm ("clz  %1,%0" : "=r"(code) : "r"(nucleotide & 31));
  return 31 - code;
#elif defined(__aarch64__)
  code = __builtin_clz(nucleotide & 31);  //  MacOS M1, M2
  return 31 - code;  
#else
  asm ("bsrl %1,%0" : "=r"(code) : "r"(nucleotide & 31));
  return code;
#endif
}

/*  a good number of length would be 40 so to use all 64 bits */
u_int64_t adapter::hash_sequence (string *sequence, int offset, int length) {  
  u_int64_t hash_code = 0;
  u_int64_t hash_even_code = 0;
  int min = ((int)sequence->length () < offset + length) ? sequence->length () : offset + length;
  for (int i = offset; i < min; i ++) {
    char nucleotide = sequence->at (i);
    int code;
#ifdef __arm__
    asm ("clz  %1,%0" : "=r"(code) : "r"(nucleotide & 31));
    code = 31 - code;
#elif defined(__aarch64__)
    code = __builtin_clz(nucleotide & 31);  //  MacOS M1, M2
    code = 31 - code;
#else
    asm ("bsrl %1,%0" : "=r"(code) : "r"(nucleotide & 31));
#endif
    hash_code <<= 1;
    hash_code ^= code;
    if ((i % 2) == 1) {
      hash_even_code <<= 1;
      hash_even_code ^= code;
    }
  }
  return (hash_code << 22) + hash_even_code;
}


int adapter::spsav_search (string *sequence, int init_position) {
  u_int64_t code = hash_sequence (sequence, init_position, adapter_seed_length);
  for (int i = 0; i < spsav_next_free; i ++) 
    for (int j = 0; j < MAX_PREFIX_GAPS; j ++)
      if (spsav[i].codes[j] == code) return i;  // Found it
  return -1;  //  Not found
}

bool adapter::spsav_insert (string *sequence, int init_position) {
  int position = spsav_search (sequence, init_position);
  int offset;
  float acc;
  if (position == -1) {  // Not Monitored
    /* There is still space */
    if (spsav_next_free < SPACE_SAVING_LENGTH) spsav_next_free ++; 
    position = spsav_next_free - 1; // Because values are sorted
    //  Check if this event could have affected the statistics
    if (spsav[position].count > 0)
      acc = (spsav[position].count - spsav[position].epsilon) / spsav[position].count;
    else
      acc = 1;
    if (acc < adapter_spsav_accuracy)
      spsav_history.reset (tot_seen % SPACE_SAVING_HISTORY);
    else
      spsav_history.set (tot_seen % SPACE_SAVING_HISTORY);
    spsav[position].epsilon = spsav[position].count;
    // Save only the adapter part
    spsav[position].sequence = sequence->substr (init_position); 
    for (int j = 0; j < MAX_PREFIX_GAPS; j ++) {
      spsav[position].codes[j] = hash_sequence (sequence, j + init_position, adapter_seed_length);
    }
  }
  else {
    float before, after;
    //  Check if this event could have affected the statistics
    if (spsav[position].count > 0)
      before = (spsav[position].count - spsav[position].epsilon) / spsav[position].count;
    else
      before = 1;
    after = (spsav[position].count + 1 - spsav[position].epsilon) / (spsav[position].count + 1);
    //  The case of a discarded element that becames again interesting
    if (before < adapter_spsav_accuracy && after >= adapter_spsav_accuracy)
      spsav_history.set (tot_seen % SPACE_SAVING_HISTORY);
    else
      spsav_history.reset (tot_seen % SPACE_SAVING_HISTORY);
  }
  spsav[position].count ++;
  tot_seen ++;  

  /*  keep spsav sorted  */
  for (offset = position - 1; offset >= 0; offset --) {
    if (spsav[position].count <= spsav[offset].count) break;
  }
  offset ++;  // Becames the last with lower count than position
  if (position != offset) { // Swap the elements
    space_saving_t tmp;
    tmp.sequence = spsav[position].sequence;
    for (int j = 0; j < MAX_PREFIX_GAPS; j ++) tmp.codes[j] = spsav[position].codes[j];
    tmp.count = spsav[position].count;
    tmp.epsilon = spsav[position].epsilon;
    spsav[position].sequence = spsav[offset].sequence;
    for (int j = 0; j < MAX_PREFIX_GAPS; j ++)
      spsav[position].codes[j] = spsav[offset].codes[j];
    spsav[position].count = spsav[offset].count;
    spsav[position].epsilon = spsav[offset].epsilon;
    spsav[offset].sequence = tmp.sequence;
    for (int j = 0; j < MAX_PREFIX_GAPS; j ++) spsav[offset].codes[j] = tmp.codes[j];
    spsav[offset].count = tmp.count;
    spsav[offset].epsilon = tmp.epsilon;
  }
  //  Statistics are stable since the data structure didn't change in the recent past
  if (spsav_history.count()<SPACE_SAVING_HISTORY*(1-adapter_signal_breakdown_threshold)) {
    //  Check that the space saving has some valid entry
    for (int i = 0; i < spsav_next_free; i ++) {
      float acc = (float) (spsav[i].count - spsav[i].epsilon) / spsav[i].count;
      if (acc >= adapter_spsav_accuracy) return false;
    }
    //  If you are here means that the space saving is table but the statistics are not
    //  valid. Need to try again with a smaller window size
    reset ();
    if (adapter_seed_length > 0) return true;
    else return false;  //  In this case adapter retrieval will fail
  }
  else  //  More reads are likely to improve statistics
    return true;   
}

void adapter::spsav_dump (void) {
  for (int i = 0; i < spsav_next_free; i ++) {
    cout << i << " ";
    cout << spsav[i].sequence << " ";
    cout << spsav[i].count << " ";
    //cout << spsav[i].epsilon << endl;
    cout << spsav[i].epsilon << " ";
    cout << (float) spsav[i].epsilon / spsav[i].count << endl;
  }
}

string adapter::guess_adapter (void) {
  array<int, 5> e {{0, 0, 0, 0, 0}};
  supporting_sequences = 0;
  compute_adapter_alignments (); // Define maxlength inside
  adapt_matrix.resize (maxlength, e); //  Allocate statistics vector
  for (int i = 0; i < spsav_next_free; i ++) {
    float acc = (float) (spsav[i].count - spsav[i].epsilon) / spsav[i].count;
    //cout << spsav[i].alignment_offset << " " << spsav[i].sequence << endl;
    if (acc < adapter_spsav_accuracy) continue;
    //  Counts the number of sequences contributing to the statistics
    supporting_sequences += spsav[i].count;  
    //  Update statistics
    for (int j = 0; j < (int) spsav[i].sequence.length (); j ++)
      adapt_matrix[spsav[i].alignment_offset + j][hash (spsav[i].sequence.at (j))] += spsav[i].count;
  }
  return get_adapter_sequence ();
}

void adapter::compute_adapter_alignments (void) {
  aligner *alignobj;
  int min_align_offset = 0;
  alignobj = new aligner ();
  for (int i = 1; i < spsav_next_free; i ++) {
    float acc = (float) (spsav[i].count - spsav[i].epsilon) / spsav[i].count;
    if (acc < adapter_spsav_accuracy) continue;
    // Use the most commonly represented sequence as a reference for comparison
    alignobj->fill_matrix (&spsav[0].sequence, &spsav[i].sequence);
    pair<string, int16_t> align_s = alignobj->buildAlignment (GLOBAL_HEAD);
    for (int j = 0; j < (int) align_s.first.length (); j ++) {
      if (align_s.first.at (j) == '1') spsav[i].alignment_offset ++;
      if (align_s.first.at (j) == '2') spsav[i].alignment_offset --;
    }
    if (spsav[i].alignment_offset > MAX_PREFIX_SHIFT || spsav[i].alignment_offset < -MAX_PREFIX_SHIFT) {
      spsav[i].epsilon = spsav[i].count;  // Will be excluded from adapter guess
      continue;
    }
    //  keep minimum updated. In the subsequent alignment offsets must be non negative
    if (spsav[i].alignment_offset < min_align_offset)
      min_align_offset = spsav[i].alignment_offset;
  }

  //  Shift sequences to have all affsets non negative and compute maxlength
  for (int i = 0; i < spsav_next_free; i ++) {
    float acc = (float) (spsav[i].count - spsav[i].epsilon) / spsav[i].count;
    if (acc < adapter_spsav_accuracy) continue;
    spsav[i].alignment_offset -= min_align_offset;  // aka add the abs value
    int l = spsav[i].sequence.length () + spsav[i].alignment_offset;
    if (l > maxlength) maxlength = l;
  }
  delete alignobj;  
}

void adapter::dump (int verbose_level) {
  if (verbose_level > 1) {  //  This is impossible with the command line, only for debugging
    array<char, 5> n {{'A', 'C', 'G', 'N', 'T'}};
    for (int j = 0; j < 5; j ++) {
      cout << n[j] << " ";
      for (int i = 0; i < maxlength; i ++) 
	cout << adapt_matrix[i][j] << " ";
      cout << endl;
    }
  }
  cout << "Number of random bases before the adapter " << random_characters << endl;
  cout << "Reads concurring to the adapter computation " << tot_seen << endl;
  cout << "Reads stored in the space saving structure " << supporting_sequences << endl;
}

string adapter::get_adapter_sequence (void) {
  char stong_char[5] = {'A', 'C', 'G', 'N', 'T'};
  char weak_char[5] = {'a', 'c', 'g', 'n', 't'};
  char *alphabet;
  string adapt_seq;
  int min_supporting_seqs = adapter_signal_breakdown_threshold * supporting_sequences;
  if (maxlength == 0) return adapt_seq; // Without statistics returns an empty string

  for (int i = 0; i < maxlength; i ++) {
    //  Collect statistics - max_val, max_pos and sum over a position
    int sum = adapt_matrix[i][0], max_val = adapt_matrix[i][0], max_pos = 0;
    for (int j = 1; j < 5; j ++) {
      sum += adapt_matrix[i][j];
      if (adapt_matrix[i][j] > max_val) {
	max_val = adapt_matrix[i][j];
	max_pos = j;
      }
    }
    
    int candidate; // the index in the *_char vector. 3 means N or n
    (max_val < adapter_signal_breakdown_threshold * sum) ? candidate = 3 : candidate = max_pos;
    (sum < min_supporting_seqs) ? alphabet = weak_char : alphabet = stong_char;

    if (adapt_seq.empty ()) {  //  At the beginning of the adapter sequence
      if (sum >= min_supporting_seqs) {  // Must be a strong signal
	if (alphabet[candidate] == 'N') random_characters ++;
	else adapt_seq.push_back (alphabet[candidate]);
      }
    }
    else {
      adapt_seq.push_back (alphabet[candidate]);
    }
    
  }
  return adapt_seq;
}


tuple<int, int, string> adapter::_adapter_min_length (mirDB &db, string &adapter, int tot_errors) {
  vector<string> keys;
  vector<string>::iterator it;
  aligner *alignobj;
  vector<int16_t> *c;
  tuple<int, int, string> maxlength (0, 0, "");
  int i, j, m;
  string upAdapt;  // Uppercase version of the adapter sequence
  int n = (adapter.length () < MAX_STR_LENGTH) ? adapter.length () + 1 : MAX_STR_LENGTH + 1;
  //  Capitalize adapter and use it for length computation
  for (string::iterator it = adapter.begin (); it != adapter.end (); it ++)
    upAdapt.push_back (toupper (*it));
  alignobj = new aligner (1, -1, -1);
  keys = db.keys ();  //  list of all the miRNA names
  for (it = keys.begin (); it != keys.end (); it ++) {
    //  db[*it] has type mirna_t *
    alignobj->fill_matrix (&db[*it]->sequence, &upAdapt, S2_GLOBAL_HEAD);
    c = alignobj->copy ();
    m = (db[*it]->sequence.length () < MAX_STR_LENGTH) ? db[*it]->sequence.length () + 1 : MAX_STR_LENGTH + 1;
    for (j = 0; j < n; j ++) {
      int16_t max_score = c->at (j * m);
      for (i = 0; i < m; i ++) {
	if (c->at ((j * m) + i) > max_score) max_score = c->at ((j * m) + i);
      }
      int estimated_errors = ceil ((float) (j - max_score) / 2);
      if (estimated_errors <= tot_errors) {
	if (j > get<0>(maxlength)) {
	  get<0>(maxlength) = j;
	  get<1>(maxlength) = estimated_errors;
	  get<2>(maxlength) = *it;  //  this is the miRNA name
	}
	else if (j == get<0>(maxlength)) {
	  if (estimated_errors < get<1>(maxlength)) {
	    get<1>(maxlength) = estimated_errors;
	    get<2>(maxlength) = *it;  //  this is the miRNA name
	  }
	}
      }
    }
  }
  if (get<0>(maxlength) < n - 1) { // XXX - the matrix has one more column for init.
    get<0>(maxlength) ++;   // Minimum length to have
    get<1>(maxlength) ++;   // at least these errors
  }
  return maxlength;
}
