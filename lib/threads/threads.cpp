#include "threads.hpp"

// force named semaphores for MacOS
#if defined(__APPLE__) && defined(__MACH__) 
#define USE_NAMED_SEMAPHORES
#endif

#ifdef USE_NAMED_SEMAPHORES /* OS X */
  #pragma message "Compiling using named semaphores"
#else
  #pragma message "Using unnamed semaphores"    
#endif



// -------------------------------------------------------------
// ThreadData struct containing mutex, semaphores, etc, for multithread execution 

// constructor: init thread array and the mutexes not related to the pc_buffer
ThreadData::ThreadData(int num, int verb) {
  if(num<1) throw new runtime_error("Invalid number of helper threads");
  num_threads = num;
  verbose = verb;
  thread_id = new pthread_t[num];
  xpthread_mutex_init(&mutex_cout,NULL,__LINE__,__FILE__);
  xpthread_mutex_init(&mutex_fout,NULL,__LINE__,__FILE__);
  xpthread_mutex_init(&mutex_rejected,NULL,__LINE__,__FILE__);
  if(verbose>0)
    cerr << "Using " << num << " helper threads\n";
}

// destructor: called when all threads have been joined
ThreadData::~ThreadData() {
  xpthread_mutex_destroy(&mutex_cout,__LINE__,__FILE__);
  xpthread_mutex_destroy(&mutex_fout,__LINE__,__FILE__);
  xpthread_mutex_destroy(&mutex_rejected,__LINE__,__FILE__);
  delete[] thread_id;
  if(verbose>0)
    cerr << "Threads data destroyed succesfully\n";
}



// create producer/consumer buffer, given number of elements and size of element
// also init free/data_semaphores, the consumer mutex, and pindex/cindex to 0 
void ThreadData::pc_buffer_init(size_t nmemb, size_t size) {
  buf_size = nmemb;
  buf_data_size = size;
  buffer = new int8_t[nmemb*size]; // correct number of bytes even if int8_t type 
  cindex = pindex = 0;
  free_slots = xsem_create_init(true, buf_size, __LINE__,__FILE__);
  data_items = xsem_create_init(true,        0, __LINE__,__FILE__);
  xpthread_mutex_init(&mutex_consumer,NULL,__LINE__,__FILE__);
  if(verbose>1)
    cerr << "pc_buffer of size " << buf_size << "x" << buf_data_size << " created\n"; 
}

// destroy the producer/consumer buffer
// if check is true verify that pindex and cindex are equal
// that is, everything produced has been consumed 
void ThreadData::pc_buffer_destroy(bool check) {
  if(check)
    if(cindex!=pindex) throw new runtime_error("Incorrect pc_buffer final status");
  delete[] buffer;
  // destroy all semaphores 
  xsem_create_init(false, 0, __LINE__,__FILE__);
  xpthread_mutex_destroy(&mutex_consumer,__LINE__,__FILE__);
  if(verbose>1)
    cerr << "pc_buffer destroyed: " << pindex << " items produced, " << cindex << " consumed\n";
}

// copy to the pc_buffer the item (of size buf_data_size) pointed to by v
void ThreadData::pc_buffer_write(const void *v) {
  xsem_wait(free_slots,__LINE__,__FILE__);
  // somewhat equivalent to: buffer[pindex++ % buf_size] = *v; 
  memcpy(buffer+((pindex++ % buf_size)*buf_data_size),v,buf_data_size);
  xsem_post(data_items,__LINE__,__FILE__);
}

// copy the item in buffer[condex] to the area pointed to by v
void ThreadData::pc_buffer_read(void *v) {
  xsem_wait(data_items,__LINE__,__FILE__);
  xpthread_mutex_lock(&mutex_consumer,__LINE__,__FILE__);
  // somewhat equivalent to: *v = buffer[cindex++ % buf_size]
  memcpy(v,buffer+((cindex++ % buf_size)*buf_data_size),buf_data_size);
  xpthread_mutex_unlock(&mutex_consumer,__LINE__,__FILE__);
  xsem_post(free_slots,__LINE__,__FILE__);
}


// ---------------------------- from xerrors.c

#define Buflen 100
#define Thread_error_wait 3 

// write error message associated to code en similarly to perror, but thread safe
void xperror(int en, const char *msg) {
#if defined(__APPLE__) || ((_POSIX_C_SOURCE >= 200112L) && !  _GNU_SOURCE)
  char errmsg[Buflen];
  int e = strerror_r(en, errmsg, Buflen);
  if(e!=0) {
    fprintf(stderr,"strerror_r failed: %s\n",msg);
    return;
  }
#else // gnu version 
  char buf[Buflen];
  char *errmsg = strerror_r(en, buf, Buflen);
#endif
  if(msg!=NULL)
    fprintf(stderr,"%s: %s\n",msg, errmsg);
  else
    fprintf(stderr,"%s\n",errmsg);
}


// ----- threads

int xpthread_create(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg, int linea, const char *file) {
  int e = pthread_create(thread, attr, start_routine, arg);
  if (e!=0) {
    xperror(e, "Error in pthread_create");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }
  return e;                       
}

                          
int xpthread_join(pthread_t thread, void **retval, int linea, const char *file) {
  int e = pthread_join(thread, retval);
  if (e!=0) {
    xperror(e, "Error in pthread_join");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }
  return e;
}

// mutex 
int xpthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr, int linea, const char *file) {
  int e = pthread_mutex_init(mutex, attr);
  if (e!=0) {
    xperror(e, "Error in pthread_mutex_init");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }  
  return e;
}

int xpthread_mutex_destroy(pthread_mutex_t *mutex, int linea, const char *file) {
  int e = pthread_mutex_destroy(mutex);
  if (e!=0) {
    xperror(e, "Error in pthread_mutex_destroy");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }
  return e;
}

int xpthread_mutex_lock(pthread_mutex_t *mutex, int linea, const char *file) {
  int e = pthread_mutex_lock(mutex);
  if (e!=0) {
    xperror(e, "Error in pthread_mutex_lock");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }
  return e;
}

int xpthread_mutex_unlock(pthread_mutex_t *mutex, int linea, const char *file) {
  int e = pthread_mutex_unlock(mutex);
  if (e!=0) {
    xperror(e, "Error in pthread_mutex_unlock");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }
  return e;
}


// semaphores


// creation and distruction of semaphores using named for MacOS and unnamed for Linux
// for named semaphores 
//   if b is true a new semaphore is created use num to ensure distinct names 
//   if b is false all created semaphores are destroyed (unlinked)
// for unnamed semaphores
//   if b is true a new sem is allocated 
//   if b is false there is nothing to do
// return a pointer to teh created semaphore or NULL is B is false   
sem_t *xsem_create_init(bool b, unsigned int value, int linea, const char *file)
{
#ifdef USE_NAMED_SEMAPHORES
  // -----------------------------------------------
  // MacOS: open a named semaphore
  // static variables containing pid and # created semaphores 
  char tmp[NAME_MAX+1];
  static int num=0;
  static intmax_t pid;
  // save pid once
  if(num==0) pid = (intmax_t) getpid();

  if(!b) {
    // delete all created semaphores
    for(int i=0;i<num;i++) {
      int e = snprintf(tmp,NAME_MAX,"%jd.%d",pid,i);
      if(e<0 || e>NAME_MAX) {
        xperror(errno,"Error creating semaphore name");
        fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
        sleep(Thread_error_wait);  // give some extra time to other threads 
        exit(EXIT_FAIL);
      }
      e = sem_unlink(tmp);
      if(e!=0) {
        xperror(errno,"Error deleting named semaphore");
        fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
        sleep(Thread_error_wait);  // give some extra time to other threads 
        exit(EXIT_FAIL);
      }
    }
    return NULL;
  }
  // create unique name 
  int e = snprintf(tmp,NAME_MAX,"%jd.%d",pid,num++);
  if(e<0 || e>NAME_MAX) {
    xperror(errno,"Error creating semaphore name");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // give some extra time to other threads 
    exit(EXIT_FAIL);
  }
  // open and init semaphore 
  sem_t *s = sem_open(tmp,O_CREAT| O_EXCL, S_IRUSR | S_IWUSR ,value);
  if(s==SEM_FAILED) {
    xperror(errno,"Error opening named semaphore"); 
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // give some extra time to other threads 
    exit(EXIT_FAIL);
  }
  return s;
#else
  // ---------------------------------------------- 
  // linux: allocate sem_t
  if(!b) {
    return NULL; // nothing to destroy
  }
  sem_t *s = (sem_t *) malloc(sizeof(sem_t));
  if(s==NULL) {
    xperror(errno, "malloc error");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // give some extra time to other threads 
    exit(EXIT_FAIL);
  }
  // init with value and no sharing 
  if(sem_init(s,0,value)!=0) {
    xperror(errno, "sem_init error");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // give some extra time to other threads 
    exit(EXIT_FAIL);
  }
  return s;
#endif
}


// deallocate an unnamed semaphore or close a named semaphore
int xsem_destroy(sem_t *sem, int linea, const char *file) 
{
#ifdef USE_NAMED_SEMAPHORES
  int e = sem_close(sem);
  if(e!=0) {
    xperror(e, "Error in sem_close");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // give some extra time to other threads 
    exit(EXIT_FAIL);
  }
  return e;
#else // linux: allocate sem_t
  (void) linea; // not used: no error is possible here
  (void) file;
  free(sem);
  return 0;
#endif  
}



int xsem_post(sem_t *sem, int linea, const char *file) {
  int e = sem_post(sem);
  if (e!=0) {
    xperror(e, "Error in sem_post");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }
  return e;
}

int xsem_wait(sem_t *sem, int linea, const char *file) {
  int e = sem_wait(sem);
  if (e!=0) {
    xperror(e, "Error in sem_wait");
    fprintf(stderr,"== %d == Line: %d, File: %s\n",getpid(),linea,file);
    sleep(Thread_error_wait);  // do not kill immediately other threads 
    exit(EXIT_FAIL);
  }
  return e;
}

