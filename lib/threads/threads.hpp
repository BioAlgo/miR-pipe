#ifndef THREADS_H
#define THREADS_H

#include <string>
#include <cstring>
#include <stdexcept>
#include <iostream>
#include <assert.h>   
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include <sys/stat.h> 
#include <fcntl.h>           /* For O_* constants */
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include "../../constants.hpp"

using namespace std;


// generic pointer representing an item exchanged 
// in a producer/consumer buffer
typedef void *job_unit;

// -------------------------------------------------------------
// struct containing mutex, semaphores, etc, for multithread execution 
struct ThreadData {
  int num_threads;       // number of threads
  int verbose;           // verbosity level 
  pthread_t *thread_id;  // array of threads id's used by create/join 
  // producer/consumer buffer
  int8_t *buffer;        // buffer of items to be processed by the threads
  size_t buf_size;          // size of the buffer 
  size_t buf_data_size;     // size of each element in the buffer 
  size_t cindex;            // shared consumer index
  size_t pindex;            // producer index (here not shared)
  void pc_buffer_init(size_t,size_t);
  void pc_buffer_destroy(bool);
  void pc_buffer_write(const void *in); // copy item pointed by in to buffer
  void pc_buffer_read(void *out); // copy item from buffer to memory pointed by out 
  // semaphores/mutexes for pc_buffer
  pthread_mutex_t mutex_consumer;   // mutex controlling access to the shared buffer
  sem_t *free_slots, *data_items;
  // additional mutexes
  pthread_mutex_t mutex_cout;     // mutex controlling access to cout
  pthread_mutex_t mutex_fout;     // mutex controlling access to output file
  pthread_mutex_t mutex_rejected; // mutex controlling access to rejected (trimmed) reads
  // constructor
  ThreadData(int size, int verb);
  // destructor
  ~ThreadData();
};




// --------------- from xerros.h

// thread
void xperror(int en, const char *msg);
int xpthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg, int linea, const char *file);
int xpthread_join(pthread_t thread, void **retval, int linea, const char *file);

// mutex 
int xpthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr, int linea, const char *file);
int xpthread_mutex_destroy(pthread_mutex_t *mutex, int linea, const char *file);
int xpthread_mutex_lock(pthread_mutex_t *mutex, int linea, const char *file);
int xpthread_mutex_unlock(pthread_mutex_t *mutex, int linea, const char *file);

// semaphores: creation, initialization, and destruction of named/unnamed semaphores
sem_t *xsem_create_init(bool b, unsigned int value, int line, const char *file);
int xsem_destroy(sem_t *s, int line, const char *file);
// semaphores: post/wait operations
int xsem_post(sem_t *sem, int line, const char *file);
int xsem_wait(sem_t *sem, int line, const char *file);

#endif
