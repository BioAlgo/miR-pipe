#include "fileManager.hpp"
#include <iostream>

struct sequence_t *filemanager::free_seq (struct sequence_t *seq) {
  if (seq != NULL) {
    if (seq->sequence != NULL) free (seq->sequence);
    if (seq->quality != NULL) free (seq->quality);
    free (seq);
  }
  return NULL;  /* dummy return NULL for convenience  */
}

struct sequence_t *filemanager::new_seq (void) {
  return extend_seq (NULL);
}

struct sequence_t *filemanager::extend_seq (struct sequence_t *sequence) {
  struct sequence_t *seq;
  if (sequence == NULL) {
    seq = (struct sequence_t *) malloc (sizeof (struct sequence_t));
    if (seq == NULL) {
      fprintf (stderr, "Memory error reading a new sequence\n");
      return NULL;
    }
    seq->buffer_size = BUFF_SIZE;
    seq->sequence = NULL;
    seq->quality = NULL;
    seq->label = new std::string ();
  }
  else {
    seq = sequence;
    seq->buffer_size += BUFF_SIZE;
  }
  seq->sequence = (char *) realloc (seq->sequence, (seq->buffer_size) * sizeof (char));
  if (seq->sequence == NULL) {
    fprintf (stderr, "Memory error allocating space for the sequence\n");
    return free_seq (seq);
  }
  if (filetype == FASTQ) {  /*  Fastq extend also quality buffer  */
    seq->quality = (char *) realloc (seq->quality, (seq->buffer_size) * sizeof (char));
    if (seq->quality == NULL) {
      fprintf (stderr, "Memory error allocating space for the quality string\n");
      return free_seq (seq);
    }
  }
  /*  This is the case of a structure used for fastq recycled to be used for fasta */
  if (filetype == FASTA) {  
    if (seq->quality != NULL) {
      free (seq->quality);
      seq->quality = NULL;
    }
  }
  return seq;
}


std::string filemanager::read_id (void) {
  std::size_t start_pos, end_pos;
  std::size_t found = seq_file_name.find_last_of("/\\");
  if (found == std::string::npos)  start_pos = 0;
  else start_pos = found + 1;
  found = seq_file_name.find_last_of(".");
  if (found == std::string::npos)  end_pos = seq_file_name.length ();
  else end_pos = found;
  return seq_file_name.substr (start_pos, end_pos - start_pos);
}

seqfile_t filemanager::get_filetype (void) {
  for (long int i = fmobj_offset; i < fmobj_buffer_size; i ++) {
    switch (fmobj_buffer[i]) {
    case '>':
      return FASTA;
    case '@':
      return FASTQ;
    case ' ':
      break;
    default:
      return UNKNOWN;
    }
  }
  return UNKNOWN;
}

filemanager::~filemanager () {
  switch (streamtype) {
  case TEXT:
    fclose (fmobj_pf);
    break;
  case STDIN_TEXT:  /*  Notting to do  */
    break;
  case GZIPPED:   
    gzclose (fmobj_gzpf);
    break;
  }
}

/*  initialize the islegal vector to true for the legal input characters  */
void filemanager::inputalphabet (alphabet_t alphabet, alphabet_case_t alphacase) {
  /*  initialize as no legal characters  */
  for (int i = 0; i < 256; i ++) islegal[i] = false;
  if (alphacase == UPPERCASE || alphacase == LOWERUPPERCASE) {
    islegal[(int)'A'] = islegal[(int)'C'] = true;
    islegal[(int)'G'] = islegal[(int)'T'] = true;
    islegal[(int)'U'] = islegal[(int)'N'] = true;
  }
  if (alphacase == LOWERCASE || alphacase == LOWERUPPERCASE) {
    islegal[(int)'a'] = islegal[(int)'c'] = true;
    islegal[(int)'g'] = islegal[(int)'t'] = true;
    islegal[(int)'u'] = islegal[(int)'n'] = true;
  }    
  if (alphabet == STRICT_DNA || alphabet == STRICT_RNA || alphabet == STRICT_DNA_RNA)
    islegal[(int)'n'] = islegal[(int)'N'] = false;
  if (alphabet == STRICT_DNA || alphabet == DNA)
    islegal[(int)'u'] = islegal[(int)'U'] = false;
  if (alphabet == STRICT_RNA || alphabet == RNA)
    islegal[(int)'t'] = islegal[(int)'T'] = false;
  if (alphabet == ALL) {
    if (alphacase == UPPERCASE || alphacase == LOWERUPPERCASE) {
      islegal[(int)'W'] = islegal[(int)'S'] = true; // two
      islegal[(int)'M'] = islegal[(int)'K'] = true; // two
      islegal[(int)'R'] = islegal[(int)'Y'] = true; // two
      islegal[(int)'B'] = islegal[(int)'D'] = true; // three
      islegal[(int)'H'] = islegal[(int)'V'] = true; // three
      islegal[(int)'Z'] = true;  //  zero
    }
    if (alphacase == LOWERCASE || alphacase == LOWERUPPERCASE) {
      islegal[(int)'w'] = islegal[(int)'s'] = true; // two
      islegal[(int)'m'] = islegal[(int)'k'] = true; // two
      islegal[(int)'r'] = islegal[(int)'y'] = true; // two
      islegal[(int)'b'] = islegal[(int)'d'] = true; // three
      islegal[(int)'h'] = islegal[(int)'v'] = true; // three
      islegal[(int)'z'] = true;  //  zero
    }
  }
}

/*  remap read sequence to a required case  */
void filemanager::remapcase (alphabet_case_t alphacase) {
  if (alphacase == LOWERCASE) {
    for (int i = 0; i < 256; i ++) {
      if (islegal[i]) {
	if (isupper (i)) remap[i] = tolower (i);
      }
    }
  }
  if (alphacase == UPPERCASE) {
    for (int i = 0; i < 256; i ++) {
      if (islegal[i]) {
	if (islower (i)) remap[i] = toupper (i);
      }
    }
  }
}
/*  Convert reads in RNA as in DNA  */
void filemanager::remaptoDNA (void) {
  remap[(int)'u'] = remap[(int)'t'];
  remap[(int)'U'] = remap[(int)'T'];
}
/*  Convert reads in DNA as in RNA  */
void filemanager::remaptoRNA (void) {
  remap[(int)'t'] = remap[(int)'u'];
  remap[(int)'T'] = remap[(int)'U'];
}

filemanager::filemanager (std::string &filename, alphabet_t alphabet, alphabet_case_t alphacase) {
  /*  initialize as every character maps itself  */
  for (int i = 0; i < 256; i ++) remap[i] = i;
  inputalphabet (alphabet, alphacase);
  seq_file_name = filename;
  error_status = false;
  if (seq_file_name.empty ()) streamtype = STDIN_TEXT;
  else {  // Read from file
    streamtype = TEXT;
    std::size_t found = seq_file_name.rfind (".gz");
    if (found != std::string::npos) { /*  .gz found ... */
      if (seq_file_name.length () - found == 3) { /*  ... at the end of the filename  */
	streamtype = GZIPPED; /*  Assumes that is a gzipped file  */
      }
    }
  }
  switch (streamtype) {
  case TEXT:
    if ((fmobj_pf = fopen (seq_file_name.c_str (), "r")) == NULL) {
      std::cerr << "Error opening input file\n";
      error_status = true;
      return;
    }
    break;
  case STDIN_TEXT: //  Read from stdin - can't be compressed
    fmobj_pf = stdin;
    break;
  case GZIPPED:   
    if ((fmobj_gzpf = gzopen (seq_file_name.c_str (), "r")) == Z_NULL) {
      std::cerr << "Error opening input file\n";
      error_status = true;
      return;
    }
    break;
  }
  empty_identifier = read_id ();
 
  fmobj_offset = 0;
  fmobj_finish = false;
  switch (streamtype) {
  case TEXT:
  case STDIN_TEXT: //  Read from stdin - can't be compressed
    fmobj_buffer_size = fread (fmobj_buffer, 1, BUFF_SIZE, fmobj_pf);
    /*  Check reading error  */
    if (fmobj_buffer_size < BUFF_SIZE && feof (fmobj_pf) == 0) {
      std::cerr << "Error reading input file\n";
      error_status = true;
      return;
    }
    break;
  case GZIPPED:
    fmobj_buffer_size = gzread (fmobj_gzpf, fmobj_buffer, BUFF_SIZE);
    /*  Check reading error  */
    if (fmobj_buffer_size < BUFF_SIZE && gzeof (fmobj_gzpf) == 0) {
      std::cerr << "Error reading input file\n";
      error_status = true;
      return;
    }
    break;
  }
  filetype = get_filetype ();
  if (filetype == UNKNOWN) {
    std::cerr << "Input file format not recognized\n";
    error_status = true;
  }
}


struct sequence_t *filemanager::read_next_seq (struct sequence_t *seq) {
  char next_char;
  parse_status_t parse_status = H_PRE_SI;
  long int qual_read_char = 0;  /*  Count of the number of characters read in quality score of fastq */
  bool sequence_legal_interrupt = false;  /*  True when the sequence can be finished  */
  seq->sequence_size = 0;
  seq->label->clear ();
  seq->alphabet = BASE33_QUAL;  /*  By default we suppose the modern quality alphabet  */
  if (fmobj_finish == true) return free_seq (seq);
  while (1) {
    if (fmobj_offset == fmobj_buffer_size) {
      fmobj_offset = 0;
      if (streamtype == TEXT || streamtype == STDIN_TEXT) {
	if (feof (fmobj_pf) == 1) {  /*  End of file and end of buffer  */
	  fmobj_finish = true;
	  break;
	}
	fmobj_buffer_size = fread (fmobj_buffer, 1, BUFF_SIZE, fmobj_pf);
	/*  Check reading error  */
	if (fmobj_buffer_size < BUFF_SIZE && feof (fmobj_pf) == 0) {
	  std::cerr << "Error reading input file\n";
	  return free_seq (seq);
	}
      }
      else {   /*   GZIPPED   */
	if (gzeof (fmobj_gzpf) == 1) {
	  fmobj_finish = true;
	  break;
	}
	fmobj_buffer_size = gzread (fmobj_gzpf, fmobj_buffer, BUFF_SIZE);
	/*  Check reading error  */
	if (fmobj_buffer_size < BUFF_SIZE && gzeof (fmobj_gzpf) == 0) {
	  std::cerr << "Error reading input file\n";
	  return free_seq (seq);
	}
      }  /*   Else  */
    }  /*  if (fmobj_offset == fmobj_buffer_size)  */
    next_char = fmobj_buffer[fmobj_offset];
    fmobj_offset ++;
    /*  From here on I process the sequence/header  */
    switch (parse_status) {
    case H_PRE_SI:
      switch (next_char) {
      case '>':
      case '@':
	parse_status = H_PRE_LABEL;
	break;
      case ' ':
	break;
      default:
	std::cerr << "Input file parsing error";
	error_status = true;
	return free_seq (seq);
      }
      break;
    case H_PRE_LABEL:
      switch (next_char) {
      case '\n':
	empty_identifier.assign (*seq->label);
	parse_status = SEQUENCE;
	break;
      case ' ':
	break;
      default:
	parse_status = H_LABEL;
	fmobj_offset --;  /*  Reprocess this caracter  */
	break;
      }
      break;
    case H_LABEL:
      switch (next_char) {
      case '\n':
	parse_status = SEQUENCE;
	break;
      case ' ':
	/* without break to allow spaces in fastq (instead trim in fasta)  */
	/* this will insert an extra space that will be removed in H_POST_LABEL  */
	if (filetype == FASTA) parse_status = H_POST_LABEL;
      default:
	seq->label->push_back (next_char);
	break;
      }
      break;
    case H_POST_LABEL: /*  happens only with filetype == FASTA  */
      if (next_char == '\n') {
	parse_status = SEQUENCE;
	//  Remove the extra space due to the absence of the break in the H_LABEL case 
	seq->label->pop_back ();
      }
      break;
    case SEQUENCE:
      switch (next_char) {
      case ' ':
	break;
      case '\n':
	sequence_legal_interrupt = true;
	break;
      case '>':  /*  No break because in fastq the caracter is evaluated  */
	if (filetype == FASTA) {
	  if (sequence_legal_interrupt == true) {
	    fmobj_offset --;  /*  Reprocess this caracter  */
	    return seq;
	  }
	  else {
	    std::cerr << "Input file parsing error";
	    error_status = true;
	    return free_seq (seq);
	  }
	}
      case '+':  /*  No break because in fasta the caracter is evaluated  */
	if (filetype == FASTQ) {
	  if (sequence_legal_interrupt == true) parse_status = FQ_PLUS;
	  else {
	    std::cerr << "Input file parsing error";
	    error_status = true;
	    return free_seq (seq);
	  }
	  break;
	}
      default:
	sequence_legal_interrupt = false; 
	/*   Buffer extension is required here  */
	if (seq->sequence_size == seq->buffer_size - 1) {  
	  if (extend_seq (seq) == NULL) {
	    std::cerr << "Error: memory allocation failure" << std::endl;
	    return NULL;
	  }
	}
	/*  check that the character is in a legal alphabet */
	if (islegal[(int)next_char] == false) {
	  std::cerr << "Error: illegal character in sequence" << std::endl;
	  return NULL;
	}
	/*  Store the remapped character  */
	seq->sequence[seq->sequence_size] = remap[(int)next_char];
	seq->sequence_size ++;
	seq->sequence[seq->sequence_size] = '\0';
	break;
      }
      break;
    case FQ_PLUS:
      if (next_char == '\n') parse_status = FQ_SCORE;
      break;
    case FQ_SCORE:
      switch (next_char) {
      case '\n':
	sequence_legal_interrupt = true; 
	break;
      /* without break - if it is not the new element it is part of the score  */	
      case '@': 
      case ' ': 
	if (qual_read_char == seq->sequence_size) {  //  Expected length
	  if (sequence_legal_interrupt == true) {    //  Comes after \n
	    fmobj_offset --;  /*  Reprocess this caracter  */
	    return seq;
	  }
	}
      default:
	sequence_legal_interrupt = false;
	if (next_char > 'K') seq->alphabet = BASE64_QUAL;  //  Can't be base33 alphabet
	if (qual_read_char < seq->sequence_size) {
	  seq->quality[qual_read_char] = next_char;
	  seq->quality[qual_read_char + 1] = '\0';
	}
	if (qual_read_char > seq->sequence_size) { //  Too long
	  std::cerr << "Error: quality length does not match read length\n";
	  return free_seq (seq);
	}
	qual_read_char ++;
	break;
      }
      break;
    }
  }
  if (filetype == FASTQ) { /* Check length equality of quality and read  */
    if (qual_read_char != seq->sequence_size) {
      std::cerr << "Error: quality length does not match read length\n";
      return free_seq (seq);
    }
  }
  return seq;
}

bool filemanager::fail (void) {
  return error_status;
}

struct sequence_t *filemanager::next_seq (struct sequence_t *seq) {
  struct sequence_t *sequence;
  // If something went wront in the initialization the method just return NULL
  if (error_status == true) return free_seq (seq);	    
  if (seq == NULL) sequence = new_seq ();
  else sequence = seq;
  if (sequence == NULL) return NULL;
  return read_next_seq (sequence);
}
