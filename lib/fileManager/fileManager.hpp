#ifndef FILEMANAGER_H_
#define FILEMANAGER_H_

#include <string>
#include <zlib.h>
#include <cctype>
#define BUFF_SIZE 4194304

typedef enum {UNKNOWN, FASTA, FASTQ} seqfile_t;
typedef enum {TEXT, STDIN_TEXT, GZIPPED} stream_type_t;
typedef enum {H_PRE_SI, H_PRE_LABEL, H_LABEL, H_POST_LABEL, SEQUENCE, FQ_PLUS, FQ_SCORE} parse_status_t;
typedef enum {STRICT_DNA, DNA, STRICT_RNA, RNA, STRICT_DNA_RNA, DNA_RNA, ALL} alphabet_t;
typedef enum {LOWERCASE, UPPERCASE, LOWERUPPERCASE} alphabet_case_t;
typedef enum {BASE33_QUAL, BASE64_QUAL} quality_alphabet_t;
struct sequence_t {
  std::string *label;  /*  header  */
  char *sequence;
  char *quality;  /*  Allocated only for fastq - same size of sequence  */
  long int buffer_size;  /*byte size fed to malloc*/
  long int sequence_size;
  quality_alphabet_t alphabet;  /*  If ambiguous is set as BASE33 */
};

class filemanager {
  std::string seq_file_name; //  The name (and path) of fasta/fastq  - empty if stdin
  std::string empty_identifier;  /*  Used when a sequence has no identifier  */
  seqfile_t filetype;
  stream_type_t streamtype;
  bool error_status;  // true if something was wrong. Next_seq return NULL in case
  char fmobj_buffer[BUFF_SIZE];
  long int fmobj_buffer_size;   /*  Buffer size for malloc  */
  long int fmobj_offset;
  FILE *fmobj_pf;     /*  Handle for standerd file  */
  gzFile fmobj_gzpf;  /*  Handle for gzipped file  */
  bool fmobj_finish;  /*  True if no more sequence is to be read  */
  bool islegal[256];  /*  True if the character is legal in the sequence  */
  char remap[256];    /*  Conversion table for legal characters  */ 
  std::string read_id (void);
  seqfile_t get_filetype (void);
  /*  if seq is not null reuse it and return it, otherwise allocate a new seq  */
  struct sequence_t *read_next_seq (struct sequence_t *seq);
  struct sequence_t *new_seq (void);
  struct sequence_t *extend_seq (struct sequence_t *sequence);

public:
  filemanager(std::string &filename, alphabet_t alphabet=DNA_RNA, alphabet_case_t alphacase=LOWERUPPERCASE);
  ~filemanager ();
  void inputalphabet (alphabet_t alphabet, alphabet_case_t alphacase);
  void remapcase (alphabet_case_t alphacase);
  void remaptoDNA (void);
  void remaptoRNA (void);
  bool fail (void);
  struct sequence_t *next_seq (struct sequence_t *seq);
  struct sequence_t *free_seq (struct sequence_t *seq);
  
};
#endif /* FILEMANAGER_H_ */
