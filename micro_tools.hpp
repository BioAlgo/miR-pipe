
void *consumer_process_single_read(void *v)
{
  th_params *a = (th_params *) v;
  
  a->tot_reads = 0;
  singleRead r;
  while(true) {
    // read dna and q from buffer     
    a->td->pc_buffer_read(&r);
    if(r.dna==NULL) break;
    process_single_read(&r,a->args);
    free(r.dna); if(r.quality) free(r.quality);  
    a->tot_reads++;
    if(a->args->verbose>1 && a->tot_reads%500000==0)
        fprintf(stderr,"[%3d] [%jd reads] %jd seconds\n", a->id, (intmax_t)a->tot_reads, (intmax_t)(time(NULL)-a->args->start_time));
  }
  if(a->args->verbose>0)
    fprintf(stderr,"[%3d] [%jd reads] %jd seconds\n", a->id, (intmax_t)a->tot_reads, (intmax_t)(time(NULL)-a->args->start_time));
  return NULL;
}




// --------------- local tools -----------------------------

// build trie from the mirna sequences stored in args.id2name
// Note that a sequence is added to the trie only if it is minimal, that is,
// it contains no other mirna sequences  
static TrieNode *build_trie(Args &args, bool revcompl)
{
  // init trie
  TrieNode *root = new TrieNode();
  // main loop reading sequences from db
  for(unsigned i=0; i<args.minimal_ids.size (); i++) {
    unsigned int id_i = args.minimal_ids[i];
    string seqname = args.db[id_i]->name;
    string seq = args.db[id_i]->sequence;
    if(revcompl)
      seq = StringTools::revcompl(seq,true); 
    if(args.verbose>2)
      cerr << "Inserting " << seq << " [" << id_i << " " << seqname << "]\n";
    root->add_string(seq.c_str(), id_i);
  }
  if(args.verbose>0) {
    cerr << args.minimal_ids.size () << ((revcompl)?" revcompl":"") <<" sequences inserted in trie\n";
    cerr << "total number of nodes: " << root->tot_nodes << endl;
    cerr << "Number of non-leaf strings: " << root->count_prefixes() <<endl;
  }
  return root;
}

