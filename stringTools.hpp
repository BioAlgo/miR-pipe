#include <string.h>
#include <iostream>
#include <cassert>
#include <stdexcept>

using namespace std;

// miscellaneous string functions
struct StringTools {
  // Convert uppercase DNA string to uppercase RNA for output
  static string asRNA (const string& s) {
    string r(s);
    for (unsigned int i=0;i<r.size(); i++) 
      if (s[i] == 'T') r[i] = 'U';
    return r;
  }
  // Compute reverse (possibly complemented) of a DNA string
  static string revcompl(const string& s, bool complement) 
  {  
    string r(s);
    for(unsigned i=0;i<r.size(); i++) {
      char c = s[s.size()-(i+1)];
      if (complement) {
        if(c=='A') r[i] ='T';
        else if(c=='T') r[i]='A';
        else if(c=='G') r[i]='C';
        else if(c=='C') r[i]='G';
        else r[i] = c;
      }
      else r[i]=c;
    }
    return r;
  }
    
  //  Not standard hamming: gaps outside the read are penalized (offset can be negative)
  static pair<u_int16_t, u_int16_t> mismatches_rm (const char *read, const char *quality, int offset, string &micro, int *mism=nullptr, int n=0) {
    int mism_pos = 0;
    pair<u_int16_t, u_int16_t> hamm_gap;
    hamm_gap.first = 0;  //  This is the hamming distance
    hamm_gap.second = 0;  //  This is the sum of qualities of mismatched positions
    int read_length = strlen (read);
    for (int i = 0; i < (int) micro.length (); i ++) {
      //  outside read
      if (i + offset < 0 || i + offset >= read_length) hamm_gap.first ++;  
      else {
  if (read[i + offset] != micro.at (i)) {
    // Copy in the mism vector (if any) the first n mismatching read offsets
    if (mism_pos < n && mism != nullptr) mism[mism_pos ++] = i + offset;
    hamm_gap.first ++;
    hamm_gap.second += quality[i + offset];
  }
      }
    }
    return hamm_gap;
  }
};

// struct representing a string match up to a given Hamming Distance  
struct HDmatch {
  int start;      // starting position of the match 
  int len;        // length of the pattern
  int hammingD;   // hamming distance 
  int id;         // sequence id
  bool revcompl;  // micro is reverse complemented?
  bool best_match;// micro is (one of) best match with the read
  int gap_score;  // contains the sum of the qualities of mismatched position (init -1)
  
  // constructor without id 
  HDmatch(int s, int z, int m) {
    start = s; len = z; hammingD = m; best_match = false; gap_score = -1;
  }
  // constructor with id and revcompl
  HDmatch(int s, int z, int m, int i, bool rc) {
     start = s; len = z; hammingD = m; id=i; revcompl=rc; best_match = false; gap_score = -1;
  }
  // constructor with id revcompl and gap_score
  HDmatch(int s, int z, int m, int i, bool rc, int gs) {
     start = s; len = z; hammingD = m; id=i; revcompl=rc; best_match = false; gap_score = gs;
  }
  //\\ set id and revc+compl status for an existing match 
  void setIdRc(int i, bool rc) {id=i;revcompl=rc;}
  
  // comparison operator: hamming is the primary key, -len the secondary: 
  // we prefer matches with fewer errors or with larger lengths
  bool operator < (const HDmatch& x) const
  {
    if(hammingD == x.hammingD)
      return(len>x.len);
    return hammingD < x.hammingD;    
  }

  bool operator == (const HDmatch& x) const
  {
    if(hammingD == x.hammingD && len == x.len) return true;
    return false;    
  }
};

// ---- overloading <<  for a single HDmatch 
std::ostream& operator<< (std::ostream& out, const HDmatch& m)
{
  out << "start: " << m.start << " len: " << m.len << " mism: " << m.hammingD << " id: " << m.id;
  return out;
}

// fast search with mismatches only, using the shift-or algorithm;  
// see https://en.wikipedia.org/wiki/Bitap_algorithm
// Note: in the shift-or representation 0 means match, 1 mismatch
struct Agrep {

  // init the bitmask used by the shift-or/agrep algorithm 
  // for each character c mask[c] is seen as a length-64 bitvector 
  // such that mask[c][i]==0 iff p[i]==c
  static void init_mask(const char *p, int m, uint64_t *mask) {
    // check pattern size
    assert(m==(int)strlen(p));
    if(m>Max_agrep_pattern_len) // position 0 is a dummy position used internally by the shift-or algorithm
      throw new  std::runtime_error("Agrep::init_mask: Pattern larger than Max_agrep_pattern_len chars");
    // init mask for shift-or search
    for(int i=0;i<256;i++) 
      mask[i]=~0; // all ones
    for(int i=0;i<m;i++) 
      mask[(int) p[i]] &= ~(1ul<<i); // set to 0 entries corresponding to p[i]
    return;
  }

  // search for p[0,m-1] inside t[0,n-1] with up to maxerr mismatches  
  // if called with skip>0 the first skip chars of p can be skipped with no penalty  
  static int search(const char *t, const char *p, int maxerr, int skip, vector<HDmatch> &found)
  {
    // get pattern size
    int m =  strlen(p);
    // create and init mask for pattern p
    uint64_t mask[256];
    init_mask(p,m,mask);
    // do the search   
    if(skip>0)
      return search_mask_skip(t,p,mask,m,maxerr,skip,found);
    else
      return search_mask(t,p,mask,m,maxerr,found);
  }


  // ------------- shift-or search with mismatches only --------------------

  // search inside t[] for matches with hamming distance up to maxerr
  // the pattern is given as the set of mask bitvectors (one per char) 
  // used by the algorithm, plus its length m
  // Use this function for multiple searches with the same pattern 

  // Algorithm explanation:
  //   R[k][j]  =  the j-th bit of R[k] at the end of the i-th (current) iteration
  //   R'[k][j] =  the j-th bit of R[k] at the end of the (i-1)-th (previous) iteration 
  // at the end of iteration i: 
  //   R[k][j+1] == 0 iff there is a match between p[0..j] and t[..i] (ie a suffix of t[0,i])
  //                      with at most k errors 
  // For k>0, we have a match with <=k errors between p[0..j] and t[..i] iff
  //   R[k][j+1] ==0 iff R'[k-1][j]==0   (MISM: k-1 errors between p[0..j-1] and t[..i-1])
  //                 OR  R'[k][j]==0 & t[i]==p[j] (MATCH: k errors between p[0..j-1] and t[..i-1] and p[j]==t[i])
  // in the code below when we are computing R[k]:
  //   oldRd1   is R'[k-1]   (values from previous iteration and previous column)
  //   R[k]     is R'[k]     (values from previous iteration) 
  // hence
  //  R[k][j+1] = oldRk1[j] & (oldRk[j] | mask[t[i]][j] )
  // working on all j simultaneously:
  //  R[k] =  oldRd1<<1 & (R[k]|mask[t[i]]))<<1;
  static int search_mask(const char *t, const char *p, const uint64_t *mask, int m, int maxerr, vector<HDmatch> &found)
  {
    int matches=0;
    if(m>Max_agrep_pattern_len) // position 0 is a dummy position used internally by the algorithm
      throw new  std::runtime_error("Agrep::search_mask: Pattern longer than Max_agrep_pattern_len chars");
    assert(maxerr<=Max_agrep_errors);

    // -- search with mismatches:
    // R[k][1,m] is the current match status allowing up to k errors 
    // a match of p[0,m-1] ending in t[i] corresponds to a 0 in R[k][m] at the end of iteration i
    // R is defined as a 1-dimensional uint64_t array since each uint64 is seen as a 64-bit array 
    uint64_t R[Max_agrep_errors+1];
    for(int k=0;k<=Max_agrep_errors;k++) // was maxerr
      R[k] = ~1; //R[k][0]=0 (match), R[k][1,m]=1 (mismatch) note m<=63
    // scan text positions
    int i;  
    for(i=0;t[i]!='\0';i++) {
      bool imatches = false;   // match found ending at position i?  
      // update R[0][0,m]
      uint64_t old_Rd1 = R[0];
      R[0] |= mask[(int) t[i]]; // R[j] = (R[j] | (p[j]==t[i] ? 0 : 1))
      R[0] <<= 1; // note: after the shift R[0][0]=0
      if((R[0] & (1ul<<m)) ==0) { //if R[0][m]==0 match with no errors ending at position i
        assert(i>=m-1);           // we must have seen t[0..m-1]
        found.push_back(HDmatch(i-m+1,m,0));// i-m+1 is the starting position of the match   
        imatches = true; // do not report other matches at position i
        matches++;
        cerr << "Unexpected exact match in agrep\n"; // not expecting exact matches here
        cerr << "Read: " << t << endl;
        cerr << "Micro: " << p << endl; 
      }
      // update R[k][0,m]
      for(int k=1; k <= maxerr; k++) {
        uint64_t tmp= R[k];
        R[k] = (old_Rd1 & (R[k] | mask[(int)t[i]])) << 1;
        old_Rd1 = tmp;
        // is this a match with minimal number of errors? 
        if(!imatches && ((R[k] & (1ul<<m)) ==0)) {
          assert(i>=m-1);           // we must have seen t[0..m-1]
          HDmatch match(i-m+1,m,k); // match with t[i-m+1..i]
          found.push_back(match);
          imatches = true;   // match ending at position i found. do not report others (will have more erros)  
          matches++;
          // return matches; // uncomment this to stop after the first match, but it could be non-minimal
        }
      }
    }
    // ------- check if a proper prefix of the pattern matches a suffix of t
    // note: i is length of t since t[i]==0 
    for(int j = 1;j<=maxerr;j++)        // j is the length of the unmatched region 
      for(int k=0;k+j<=maxerr;k++)      // k is the number of errors in the prefix of p[]
        if( (R[k] & (1ul<<(m-j))) ==0) {   // match with k errors involving m-j pattern chars
          HDmatch match(i-m+j,m,k+j);   // match with t[i-m+j..i+j-1]
          found.push_back(match);
          matches++;
          break;  // no need to increase k, consider next prefix 
        }
    return matches;
  }

  // search with up to k mismatches, possibly ignoring up to s initial chars of the pattern p[]
  // note that the skipped chars do count in the total number of mismatches 
  static int search_mask_skip(const char *t, const char *p, const uint64_t *mask, int m, int maxerr, int s,  vector<HDmatch> &found)
  {
    int matches=0;
    assert(m>s);   // pattern must be longer than skipped chars
    if(m>Max_agrep_pattern_len)       // position 0 is a dummy position used internally by the algorithm
      throw new  std::runtime_error("Agrep::search_mask_skip: Pattern longer than Max_agrep_pattern_len chars");
    assert(maxerr<=Max_agrep_errors);

    // ---- search with mismatches:
    // R[k][1,m] is the current match status allowing up to k errors 
    // a match of p[0,m-1] ending in t[i] corresponds to a 0 in R[k][m] at the end of iteration i
    // R is defined as a 1-dimensional uint64_t array since each uint64 is seen as a 64-bit array 
    // --- the skip parameter:
    // setting R[k][1,s] = 0 mimic, for j=1..s-1  a match between p[0...j] and t[-j ... -1]
    // hence a match with no errors can start with any p[j] with j<=s
    uint64_t R[Max_agrep_errors+1];
    for(int k=0;k<=Max_agrep_errors;k++) { // maxerr
      R[k] = ~1; //R[k][0]=0 (match), R[k][1,m]=1 (mismatch) note m<=Max_agrep_pattern_len
      R[k] = R[k]<<s; // now R[k] = 0^{1+s} 1^{63-s} emulating a match with t[-s] ... t[-1]
    }
    // scan text positions 
    int i;
    for(i=0;t[i]!='\0';i++) {
      bool imatches = false;  // match found ending a position i?
      // update R[0][0,m]
      uint64_t old_Rd1 = R[0];
      R[0] |= mask[(int) t[i]]; // R[j] = (R[j] | (p[j]==t[i] ? 0 : 1))
      R[0] <<= 1; // note: after the shift R[0][0]=0
      if((R[0] & (1ul<<m)) ==0) { //if R[0][m]==0 match with no errors ending at position i
        int start =  i-m+1;               // i-m+1 is the starting position of match
        int skip = start>=0 ? 0 : -start; // chars to be skipped from t+start and p
        assert(skip>=0 && skip<= s);      // at most s chars to be skipped
        HDmatch match(start,m,skip);
        found.push_back(match);  
        imatches = true;// match ending at position i found. do not report others (will have more erros)  
        matches++;
      }
      // update R[k][0,m]. If imatches==true update R[] but do not report other matches ending in t[i]
      for(int k=1; k <= maxerr; k++) {
        uint64_t tmp= R[k];
        R[k] = (old_Rd1 & (R[k] | mask[(int)t[i]])) << 1;
        old_Rd1 = tmp;
        // is this a match with minimal number of errors? 
        if(!imatches && ((R[k] & (1ul<<m)) ==0)) {
          int start =  i-m+1;               // starting position of match
          int skip = start>=0 ? 0 : -start; // chars to be skipped from t+start and p
          assert(skip>=0 && skip<= s);      // at most s chars to be skipped
          HDmatch match(i-m+1,m,k+skip);    // total number of errors is k+skipped 
          found.push_back(match);
          imatches = true;
          matches++;
        }
      }
    }

    // ------- check if a proper prefix of the pattern matches a suffix of t
    // note: i is length of t since t[i]==0.
    // note we are considering also the case skip>0 which is unlikely (read strictly inside micro)
    for(int j = 1;j<=maxerr;j++)        // j is the length of the unmatched region 
      for(int k=0;k+j<=maxerr;k++)      // k is the number of errors in the prefix of p[]
        if( (R[k] & (1ul<<(m-j))) ==0) {   // match with k errors involving m-j pattern chars
          int start =  i-m+j;               // starting position of match
          int skip = start>=0 ? 0 : -start; // chars to be skipped from t+start and p
          HDmatch match(i-m+j,m,k+j+skip);   // match with t[i-m+j..i+j-1]
          found.push_back(match);
          matches++;
          break;  // no need to increase k, consider next prefix 
        }
    return matches;
  }
  
  
  // ----------------------- edit distance ---------------------- 
  
  // search inside t[] for matches with edit distance up to maxerr
  // the pattern is given as the set of mask bitvectors (one per char) 
  // used by the algorithm plus its length m
  // If no alignment with at most maxerr errors is found return  the pair 
  // <maxerr+1,0> otherwise return the pair containing  the minimum cost 
  // of an alignment ending in pattern[m-1], and the (leftmost) ending 
  // position in t[] of such alignment
  // The search stops as soon as an exact match is found; otherwise it
  // continues until the last char of t[] in search of a better match
  
  // Algorithm explanation:
  //   R[k][j]  =  the j-th bit of R[k] at the end of the i-th (current) iteration
  //   R'[k][j] =  the j-th bit of R[k] at the end of the (i-1)-th (previous) iteration 
  // at the end of iteration i: 
  //   R[k][j+1] == 0 iff there is a match between p[0..j] and t[..i] (ie a suffix of t[0,i])
  //                      with at most k errors 
  // For k>0, we have a match with <=k errors between p[0..j] and t[..i] iff
  //   R[k][j+1] ==0 iff R'[k-1][j+1]==0 (INS t[i]: k-1 errors between p[0..j] and t[..i-1])
  //                 OR  R'[k-1][j]==0   (MISM: k-1 errors between p[0..j-1] and t[..i-1])
  //                 OR  R'[k][j]==0 & t[i]==p[j] (MATCH: k errors between p[0..j-1] and t[..i-1] and p[j]==t[i])
  //                 OR  R[k-1][j]==0    (INS p[j]: k-1 errors between p[0..j-1] and t[..i])
  // in the code below when we are computing R[k]:
  //   oldRk1   is R'[k-1]   (values from previous iteration and previous column)
  //   oldRk    is R'[k]     (values from previous iteration) 
  // hence
  //  R[k][j+1] = oldRk1[j+1] & oldRk1[j] & (oldRk[j] | mask[t[i]][j] ) & R[k-1][j]
  // working on all j simultanesouly:
  //  R[k] = oldRk1 & oldRk1<<1 & (oldRk|mask[t[i]]))<<1 & R[k-1]<<1;
  static pair<int,int> search_edit_distance(const char *t, const uint64_t *mask, int m, int maxerr)
  {
    int min_error = maxerr+1;
    size_t min_textlen = 0;
    if(m>Max_agrep_pattern_len) // position 0 is a dummy position used internally by the algorithm
      throw new  std::runtime_error("Agrep::search_edit_distance: Pattern longer than Max_agrep_pattern_len chars");
    assert(maxerr<=Max_agrep_errors);
    
    // ---- compute edit distance:
    // R[k][1,m] is the current match status allowing up to k errors 
    // a match of p[0,m-1] ending in t[i] corresponds to a 0 in R[k][m] at the end of iteration i
    // R is defined as a 1-dimensional uint64_t array since each uint64 is seen as a 64-bit array 
    uint64_t R[Max_agrep_errors+1];
    for(int k=0;k<=maxerr;k++) 
      R[k] = ~1;      //R[k][0]=0 (match), R[k][1,m]=1 (mismatch) note m<=63
    // scan text positions 
    for(int i=0;t[i]!='\0';i++) {
      // update R[0][0,m]
      uint64_t old_Rk1 = R[0];
      R[0] |= mask[(int) t[i]];   // R[0][j] = (R[0][j] | (p[j]==t[i] ? 0 : 1))
      R[0] <<= 1; // note: after the shift R[0][0]=0
      if((R[0] & (1ul<<m)) ==0) { //if R[0][m]==0 match with no errors ending at position i
        assert(i>=m-1);           // we must have seen t[0..m-1]
        min_error=0; min_textlen = i+1;
        break;
      }
      // update R[k][0,m] for the same t[i]
      for(int k=1; k <= maxerr; k++) {
        if( k>= min_error) break; // no need to go on if we cannot improve 
        uint64_t oldRk= R[k];
        // R[k] = old_Rk1 & (old_Rk1<<1) & ((oldRk|mask[(int)t[i]])<<1)  & (R[k-1]<<1);
        // same with less shift
        R[k] = old_Rk1 & ((old_Rk1 & (oldRk|mask[(int)t[i]])  & R[k-1]) <<1);
        old_Rk1 = oldRk;
        // is this a match with minimal number of errors? 
        if((R[k] & (1ul<<m)) ==0) {
          assert(i>=m-1-k);           // we must have seen t[0..m-1-k]
          min_error = k; min_textlen=i+1;
        }
      }
      if(min_textlen>0)  // remove this to go on searching for matches with fewer errors
        break;
    }
    return pair<int,int>(min_error,min_textlen);
  }
};

