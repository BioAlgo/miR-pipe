# EZcount

*ezcount* is all-in-one pipeline for microRNA expression quantification from NGS sequencing data.

Copyrights 2020- by the authors. 
 

## Documentation

The manual page is available in [pdf format](man/ezcount.pdf) or on the command line typing `man man/ezcount.1` (or simply `man ezcount` after a system-wide installation)

## Requirements 

*ezcount* is written in C/C++; it has been tested with `g++` on Ubuntu and MacOS but it should compile with any modern C++11 ready compiler. 

## Installation

* Download/Clone the repository
* `make` (create the C/C++ executables) 
* `sudo make install` (optional, to do a system-wide installation)
* `ezcount -h` (get usage instruction)


## Sample usage

The following examples use files from the `example` directory and should execute without errors if the compilation was successful.

To create the database file `test.db` from the GFF file `examples/micro.gff3` and the reference `examples/reference.fa.gz` type:
```
ezcount -d test.db -g examples/micro.gff3 -r examples/reference.fa.gz
```

To compute the expression quantification of the miRNA sequences in `test.db` for the reads contained in `examples/input.fq.gz` writing 
the annotated reads to `output.fq` type:
```
ezcount -d test.db -f examples/input.fq.gz -o output.fq
```


## References

\[1\] Filippo Geraci, Giovanni Manzini, *EZcount: an All-in-one Software for microRNA Expression Quantification from NGS Sequencing data* IIT TR-12/2020

\[2\] Filippo Geraci, Giovanni Manzini, *EZcount: an All-in-one Software for microRNA Expression Quantification from NGS Sequencing data* Computers in Biology and Medicine, Vol. 133 (2021). Doi: [10.1016/j.compbiomed.2021.104352](https://authors.elsevier.com/a/1cuCT2OYcyCtt)



