.TH man 1 "17 March 2021" "1.0" "ezcount man page"
.SH NAME
.B ezcount
\- all in one pipeline for microRNA quantification.
.SH SYNOPSIS
.B ezcount
.RB  \-d
.IR input.db 
.RB  \-f
.IR reads.fq(.gz) 
.RB [ OPTIONS ]

.B ezcount
.RB  \-r 
.IR reference.fa(.gz) 
.RB  \-g
.IR mirna.gff 
.RB  \-d
.IR output.db  
.RB [ OPTIONS ]


.SH DESCRIPTION
.B ezcount  
is an all in one pipeline for microRNA quantification.
.B ezcount 
takes as input a fastq file with the reads and a microRNA database 
and returns the number of
reads matching every microRNA. Library specific parameters (i.e. the
adapter sequence) can be either auto determined or passed in the command line.

If the microRNA database is not available it can be created with the second mode of operation where the input consists of a reference file and the GFF description of mature microRNA. The microRNA database is created using this information and, if a read file is provided, the counting process is carried out as above.




.SH OPTIONS
.SS "Input files"


.TP
.BI \-f " FILE" "\fR,\fP \-\^\-fastq " FILE
process reads in the fastq file
.IR FILE.
Reads sequences can be either uppercase or lowercase or mixed, both
RNA and DNA alphabets can be supplied.
.B ezcount
seamlessly performs all the necessary internal conversions.
.IR FILE
can be compressed in gzip format, in this case the extension .gz
is required. 

.TP
.BI \-r " FILE" "\fR,\fP \-\^\-reference " FILE
load from the multi fasta file
.IR FILE
the sequences of the chromosomes of the reference genome and extract
from them the sequences of the microRNAs. The file
.IR FILE
can be compressed in gzip format, in this case the extension .gz is
required. The reference genome
.IR FILE
can contain Ns, mix lowercase and uppercase characters and even be
encoded with the RNA alphabet instead of DNA. This option must be used
in conjunction with
.B -g.

.TP
.BI \-g " FILE.gff" "\fR,\fP \-\^\-mir-gff " FILE.gff
read from the GFF (General Feature Format) file
.IR FILE
the description of mature microRNAs. The GFF file should consist in a
9 columns tab separated file where mature microRNAs are identified
with the label "miRNA" in the column
.I feature
(namely the third column).
Following the gff3 specifications, coordinates in
.IR FILE
must be 1-based. Comment lines (beginning with "#") are ignored.
microRNAs descriptions downloaded from
.IR http://www.mirbase.org/
are already in gff3 and don't need pre-processing. This option must be
used in conjunction with 
.B -r.

.SS "MicroRNA database"

.TP
.BI \-d " FILE" "\fR,\fP \-\^\-mir-db " FILE
specify the microRNA database file. To perform read counting,
.B ezcount
needs the microRNA sequences as well as other meta-data. To avoid
reading an entire genome file and recomputing this information all the
times, it is possible to store the microRNAs' description in a database
file and load it on demand. This option is used either for creating or
loading the database. When the
.B -r
and
.B -g
flags are used,
.B -d
specifies the output file where to store a database containing all the
information and meta-data describing the microRNAs, otherwise
.B -d
specifies the file name from where this information is
loaded. Manual editing of the database file is strongly discouraged
since it can cause an unpredictable behavior and an unreliable
counting. 


.SS "Output files"

.TP
.BI \-c " FILE" "\fR,\fP \-\^\-counts " FILE
output counts in the file
.IR FILE
or on the standard output if the option
.B -c
is not specified. Counts file consists in three tab separated columns:
.I miRNA,
.I count,
.I ambiguous.
Unless differently specified with the option
.B \-\^\-count-add-ambiguous,
the column
.I count
reports only unambiguously matching microRNAs, while
the column
.I ambiguous
reports the number of reads for which multiple matching are
equiprobable. Notice that the sum over the column
.I ambiguous
is higher than (at least twice) the overall number of ambiguously
matching reads (see the
.I Summary
section for more details).

.TP
.B \-\^\-count-no-header
don't write header line in output count file
.IR FILE

.TP
.B \-\^\-count-add-ambiguous
add the count of the ambiguous matching to the column
.I count
of the counts file
.IR FILE.
Unless the option 
.B \-\^\-count-hide-ambiguous
is set, the column ambiguous is still displayed.

.TP
.B \-\^\-count-hide-ambiguous
write only the first two columns (
.I miRNA
and
.I count)
of the counts file. Displayed counters values are not altered by
this option (see 
.B \-\^\-count-add-ambiguous
for details).

.TP
.BI \-o " FILE" "\fR,\fP \-\^\-out " FILE
write an annotated fastq file with (a portion of) the input reads. Unless
adapter trimming was disabled, the reported reads are trimmed,
characters are automatically turned in uppercase and converted into
the DNA alphabet. If multithreading is enabled, the ordering of the
original file is not preserved.  The headers of the reads are
annotated appending a label with the read category. There are five
possible categories:  
.RS
.TP
.B  ADAPT
no adapter was found in the read. No further searching will be done on
the read. 
.TP
.B MISS
after successful adapter trimming (unless disabled), no microRNA was
found. 
.TP
.B MATCH
after successful adapter trimming (unless disabled) an unambiguous
matching with a microRNA was found. A match does not necessarily mean
that only one microRNA matched the read, but in case of multiple
matching 
.B ezcount 
was able to unambiguously identify the correct match. After the label
.B MATCH
, a quadruple (separated with semicolon ":") reports details about
the matching microRNA. The quadruple contains: the name of the
microRNA, the 0-based starting position of the match, the direction of
the match ("-" means the microRNA matched the read in reverse
complement), and the hamming distance between the read and the
microRNA. 
.TP
.B AMB
after successful adapter trimming (unless disabled) at least two
alternative matchings have been found. In this case the ambiguity
couldn't be solved.
.TP
.B SHORT
after successful adapter trimming (unless disabled) the reminder of
the read was too short to match any microRNA in the database. Notice
that the minimum length of a read is computed according with the
searching parameters and the list of microRNAs in the database.
.RE


.TP
.B \-\^\-out-filter-adapter
don't report reads without adapter (otherwise labelled with
.B  ADAPT
) in the output fastq
.IR FILE
specified with the
.B  -o
option.

.TP
.B \-\^\-out-filter-mismatch
don't report mismatching reads (otherwise labelled with
.B MISS
) in the output fastq
.IR FILE
specified with the
.B  -o
option.


.TP
.B \-\^\-out-filter-match
don't report matching reads (otherwise labelled with
.B MATCH
) in the output fastq
.IR FILE
specified with the
.B  -o
option. 

.TP
.B \-\^\-out-filter-ambiguous
don't report ambiguously matching reads (otherwise labelled with
.B AMB
) in the output fastq
.IR FILE
specified with the
.B  -o
option. 

.TP
.B \-\^\-out-filter-short
don't report reads too short to match any microRNAs (otherwise labelled with
.B SHORT
) in the output fastq
.IR FILE
specified with the
.B  -o
option. The minimum length of a read cannot be directly set but it is
computed on the basis of the content of the microRNA db, the allowed
mismatches (
.B \-M
option), and the allowed skipped initial characters (
.B \-S
option).

.TP
.B \-A \fR \fP \-\^\-out\-RNA
use the RNA (A,C,G,U,N) alphabet in the output fastq
.IR FILE.
If not specified the output reads will be saved using the DNA
(A,C,G,T,N) alphabet regardless the input format.

.SS "Searching parameters"

.TP 
.BI \-M " INT" "\fR,\fP \-\^\-mismatch " INT 
maximum number of allowed mismatches [default=2, max=3]

.TP 
.BI \-S " INT" "\fR,\fP \-\^\-skipped " INT 
maximum number of allowed initial skipped characters [default=0,
max=2]

.TP 
.BI \-E " INT" "\fR,\fP \-\^\-adapter\-edit " INT 
maximum adapter/read edit distance [default=2, max=3]

.TP 
.B \-R \fR\fP \-\^\-reverse\-complement 
match also reverse-complement (this option can double the overall
running time).

.SS "Adapters"
By default,
.B ezcount
attempts to auto-determine the adapter sequence. This is achieved by
searching perfectly matching microRNAs within the reads and collecting
statistics about the downstream content. Once these statistics are
collected,
.B ezcount
computes a consensus string and automatically select a prefix long
enough to ensure that the adapter cannot be confused by chance with a
microRNA according with the searching parameters. However, when the
adapter is somehow too similar to a microRNA, this procedure can
return with an error. In this case, the user can still remove the adapter 
supplying the sequence, or perform the counting on the entire read.
Once the adapter sequence is determined,
.B ezcount
reports it on the standard error. The sequence is always returned
using the DNA alphabet, uppercase is used for characters which were
determined with sufficient confidence, while lowercase
(which in general should be concentrated at the end of the adapter
sequence) corresponds to not statistically well supported characters.
.B ezcount
computes also the minimum length to ensure the edit distance of the
adapter sequence with any microRNA to be higher than the threshold
specified with the
.B -E
option. If this measure results to exceed the adapter length,
.B ezcount
returns with an error.

.TP  
.BI \-a " STRING" "\fR,\fP \-\^\-force\-adapter " STRING 
provide adapter sequence to trim. The sequence
.IB STRING
can either use the DNA or the RNA alphabet and mix lowercase and
uppercase characters. 
.B ezcount
automatically performs the appropriate internal conversions. If the
edit distance between the provided sequence and any microRNA is lower
than the threshold provided with the
.B -E
option,
.B ezcount
returns with an error.

.TP 
.B \-R \fR\fP \-\^\-no\-trim\-adapter
disable adapter trimming before counting. This option affects
downstream counting in several ways
.RS
.IP \[bu] 
searching with gaps on the entire read is typically slower than
removing the adapter and searching on the trimmed read;
.IP \[bu]
reads that would be excluded because the adapter would not be
recognized, will be included in the counts;
.IP \[bu]
it could be impossible to disambiguate microRNAs that are one 
subsequence of the other (i.e. hsa-miR-5692c has the same sequence of
hsa-miR-5692b with an extra final AC. There are cases where
.B ezcount
may need to use the adapter information to disambiguate these two microRNAs).
.RE

.TP 
.BI \-\^\-adapter\-seed\-length " INT"
set the size of the neighborhood of perfectly matching microRNAs used
to collect statistics for adapter guess. This parameter does not
affect the final adapter length. A larger value can reduce the number
matching microRNAs required for statistics unless it exceeds the read
length too much. In this case, however,
.B ezcount
self-adjust this parameter unless the user supplies a specific value. 
Changing this parameter is not recommended unless you are perfectly
aware of what you are doing. [default=40]

.SS "Other parameters"

.TP 
.BI \-t " INT" "\fR,\fP \-\^\-threads " INT 
maximum number of threads used. [default=number of
hardware thread contexts as reported by
thread::hardware_concurrency()]

.TP 
.B \-\^\-disable\-mirna\-count
microRNAs are not searched, only adapter trimming is done.

.TP
.B \-T \fR \fP \-\^\-trimmer
alias for
.B --disable-mirna-count --out-filter-short --out-filter-adapter.
This option could be useful to pre-process the input fastq for
subsequent alignment with other software.

.TP
.BI \-l " INT" "\fR,\fP \-\^\-max-reads " INT
only the first
.I INT
reads will be processed. To guess the adapter this value must be
high enough for the algorithm to collect statistics, otherwise the
adapter would not be guessed and the software will terminate with
error. If either the option
.B --force-adapter
or
.B --no-trim-adapter
is specified, no lower limit applies.

.TP
.BI \-w " PATH" "\fR,\fP \-\^\-cwd " PATH
set
.I PATH
as a prefix for all input and output files

.TP
.B \-v \fR \fP \-\^\-verbose 
print verbose output in the standard error

.TP
.B \-h \fR \fP \-\^\-help 
print a succint help page

.SH SUMMARY STATISTICS
At the end of the read processing,
.B ezcount
reports on the standard error synthetic statistics about trimming
and counting. The "overall processed reads" are partitioned in four
classes: reads for which 
.B ezcount
could not find the adapter (tagged with
.B ADAPT
in the output fastq), too short reads (tagged with
.B SHORT
in the output fastq), reads that could not have been matched with any
microRNA (tagged with
.B MISS
in the output fastq), and "overall matching reads" that includes
perfect and ambiguous matching (
.B MATCH
and 
.B AMB
in the output fastq). In order to discriminate these two categories,
.B ezcount
reports a specific counter of the subset of ambiguously matching
reads. Finally
.B ezcount
reports a counter of the subset of "overall matching reads" that
matched in reverse complement (if the option
.B -R
was set) and a counter of matches with no gap (namely reads that
contain the very exact microRNA sequence).


.SH EXIT STATUS
The
.B ezcount
utility exits with one of the following values:
.IP 0
if everything was OK
.IP  1
in case of error


. SH EXAMPLES
This example shows how to build a database from a reference
multi fasta file (say 
.IR reference.fa
) and a microRNA description file in gff format (say
.IR micro.gff
)

.RS 3
$ ezcount -r
.IR reference.fa
-g
.IR micro.gff
-d
.IR mirna.db
.RE

In this case the
.I -d
option specifies an output file.

This example shows a typical use of
.B ezcount
where a compressed fastq file
.IR input.fq.gz
is provided as input, the adapter is automatically determined, the
microRNA description is derived from the  
.IR mirna.db
database and the output counts are written in the file
.IR counts.tab

.RS 3
$ ezcount -d
.IR mirna.db
-f
.IR input.fq.gz
-c
.IR counts.tab
.RE

The two previous examples can be combined into a single run of
.B ezcount
thus creating the
.IR mirna.db
from the reference and performing the counting at the same time.

.RS 3
$ ezcount -r
.IR reference.fa
-g
.IR micro.gff
-d
.IR mirna.db
-f
.IR input.fq.gz
-c
.IR counts.tab
.RE

Again, in this case the
.I -d
option specifies an output file.

Another use of
.B ezcount
could be that of preprocessing a fastq file
.IR input.fq
without performing counting. Notice that, even providing the adapter
sequence instead of guessing it,
.B ezcount
still requires the microRNA database description to ensure that the
trimming does not cut sequences that are likely to belong to a
microRNA. In this example, the adapter is guessed.

.RS 3
$ ezcount -T
-d
.IR mirna.db
-f
.IR input.fq
-o
.IR output.fq
.RE


.SH BUGS
No known bugs.

.SH AUTHORS
.IP \[bu]
Filippo Geraci - Institute for Informatics and Telematics, CNR - <filippo.geraci@iit.cnr.it>
.IP \[bu]
Giovanni Manzini - Department of Computer Science, University of Pisa - <giovanni.manzini@unipi.it>

